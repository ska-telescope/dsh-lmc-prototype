import os
import sys

from tango import DeviceProxy
from typing import Dict, Any

DATABASEDS_NAME = "tango-databaseds"
CLUSTER_DOMAIN = "miditf.internal.skao.int"

def init_env(namespace: str, dish_id: str) -> Dict[str, DeviceProxy]:
    KUBE_NAMESPACE = namespace
    DISH_ID = dish_id
    
    sys.path.append("/home/jovyan/.local/lib/python3.11/site-packages")
    os.environ["TANGO_HOST"] = f"{DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.{CLUSTER_DOMAIN}:10000"    

    DISH_MANAGER_FQDN = f"mid-dish/dish-manager/{DISH_ID}"
    DISH_STRUCTURE_MANAGER_FQDN = f"mid-dish/ds-manager/{DISH_ID}"
    SPFC_FQDN = f"mid-dish/simulator-spfc/{DISH_ID}"
    SPFRX_FQDN = f"mid-dish/simulator-spfrx/{DISH_ID}"
    DISH_LOGGER_FQDN = f"mid-dish/dish-logger/{DISH_ID}"
    
    dish_manager_proxy = DeviceProxy(DISH_MANAGER_FQDN)
    ds_manager_proxy = DeviceProxy(DISH_STRUCTURE_MANAGER_FQDN)
    spfc_proxy = DeviceProxy(SPFC_FQDN)
    spfrx_proxy = DeviceProxy(SPFRX_FQDN)
    dish_logger_proxy = DeviceProxy(DISH_LOGGER_FQDN)
    
    device_proxies = {
        "dish_manager_proxy": dish_manager_proxy,
        "ds_manager_proxy": ds_manager_proxy,
        "spfc_proxy": spfc_proxy,
        "spfrx_proxy": spfrx_proxy,
        "dish_logger_proxy": dish_logger_proxy,
    }
    return device_proxies
