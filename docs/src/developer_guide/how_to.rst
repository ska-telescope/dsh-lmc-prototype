==============================
Deploy Dish.LMC with Simulators
==============================
This page will guide you through the steps to deploying DishLMC
along with the simulators locally. It is assumed here that you have cloned the
``ska-dish-lmc`` repository, and have `set up your development environment`_. 

Deploy in Minikube
^^^^^^^^^^^^^^^^^^

1. Deploy ska-tango-operator to manage the pods and their configuration.

.. tabs::

   .. tab:: ska-tango-operator chart deployment

      .. code-block:: console

        $ git clone git@gitlab.com:ska-telescope/ska-tango-operator.git
        $ cd ska-tango-operator
        $ make k8s-install-chart


2. Deploy ska-dish-lmc from the chart

.. tabs::

   .. tab:: ska-dish-lmc chart deployment

      .. code-block:: console
        
        $ cd ska-dish-lmc
        $ GITLAB_CI=false make k8s-install-chart


.. tip:: Pull the docker images into your minikube cluster prior, to make deployments faster.
   Consult :doc:`deployment section <../deployment>` if you want to add Dish LMC as a sub chart in your own repo.

The deployment will constitute pods for:

* Tango DB
* DishManager
* DSManager
* Simulators: SPF, SPFRx & OPCUA server

All these pods have to be up in the ``ska-dish-lmc`` namespace to have a
fully functional software to interact. The pod of particular interest will be
``ds-dishmanager-001-0``. The device name which connects to the DishManager server is
``mid-dish/dish-manager/SKA001``: this may change, and can be looked up in the deployment on
the ``deviceserver`` namespace by inspecting the ``Config`` value on the
``dishmanager-001`` pod (see the image below).

.. image:: ../images/dev_name_k9s.png
   :width: 100%

Manage your Deployment
^^^^^^^^^^^^^^^^^^^^^^
A deployment constitutes a number of running pods, with each deployed pod
exposing a running tango device. kubectl commands can be used to manage your
deployment; however using `k9s`_ to manager your cluster deployment is very
efficient and time saving. Having an additional (sandbox) pod to in the cluster
to drive the control and monitoring is also essential.

There's an `example configuration file`_ which can be used to deploy an
additional pod in the cluster. To use it run the kubectl command below and
itango from the pod's shell.


.. tabs::

   .. tab:: deploy sandbox

      .. code-block:: console
        
        $ kubectl apply -f sandbox.yaml
        $ itango3

The sandbox pod can be killed using ``Ctrl + k``.

.. note:: Deploy DishLMC before running itango on the sandbox pod to avoid database connection errors

.. tabs::

   .. tab:: tear down DishLMC

      .. code-block:: console
        
        $ cd ska-dish-lmc
        $ make k8s-uninstall-chart

   .. tab:: tear down tango operator

      .. code-block:: console
        
        $ cd ska-tango-operator
        $ make k8s-uninstall-chart


.. _set up your development environment: https://developer.skatelescope.org/en/latest/tools/tango-devenv-setup.html
.. _k9s: https://github.com/derailed/k9s
.. _example configuration file: https://gitlab.com/ska-telescope/ska-dish-lmc/-/blob/master/charts/sandbox.yaml?ref_type=heads
