==========================
SKA Dish LMC Documentation
==========================

Description
-----------

Dish LMC provides master control and rolled-up monitoring of dish. When
commanded, it propagates the associated command to the relevant sub-components
and updates its related attributes based on the aggregation of progress reported
by those sub-components. It also exposes some attributes of the sub-components without
connecting directly to them. Other devices forming part of the Dish Control system
(e.g. power manager or archiver) will be uploaded in the repository in the future.

.. image:: images/DishLMC.png
  :width: 100%
  :alt: Dish LMC diagram


Devices
-------

Dish LMC provides the Dish Manager TANGO device as control interface of the Dish. The TMC MID
will be interfacing with the Dish LMC using this interface. DishManager in turn interfaces with
all the sub-components to provide further montoring and control. Currently, Dish LMC implements a:

* :doc:`Dish Logger <devices/dish_logger>` (log consumer)
* :doc:`Dish Manager <devices/dish_manager>`
* :doc:`DS Mananger <devices/ds_manager>`
* :doc:`Simulators <devices/dish_simulators>`

The behaviour is verified against a set of BDD tests before integrating with the actual components.

.. image:: images/dish-lmc-with-simulators.png
  :width: 100%
  :alt: Dish LMC with Simulators


References
----------

.. toctree::
  :maxdepth: 1

  Dish LMC Devices<devices/index>
  Deployment<deployment>
  Developer Guide<developer_guide/index>
  User Guide<user_guide/index>
  API<api/index>
  CHANGELOG<CHANGELOG>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
