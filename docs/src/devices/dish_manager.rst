============
Dish Manager
============

The DishManager is a tango device that provides master control
and monitoring of the dish. When commanded, it propagates the particular
command to the applicable surbsevient devices and updates its attributes
based on the aggregation of progress reported by the surbsevient devices.

For more context on the dish manager explore the project `documentation`_.

.. _documentation: https://developer.skao.int/projects/ska-mid-dish-manager/en/latest/?badge=latest
