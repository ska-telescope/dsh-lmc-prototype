===========
Dish Logger 
===========

This device collects logs from other Dish devices and logs them to `stdout` according to the SKA logging format.
All devices that can log are configured to log to this device by default.

The following attributes are set:

* `ElementLoggerEnabledDefault` is set to `True`
* `LoggingTargetElementDefault` is set to the DishLogger device

The API of the device can be explored :doc:`here. <../api/dish_logger/index>`
