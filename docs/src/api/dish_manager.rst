============
Dish Manager
============

* `Dish Manager Device Server`_
* `Dish Manager Interface Description`_

.. _Dish Manager Device Server: https://developer.skao.int/projects/ska-mid-dish-manager/en/latest/api/devices/dish_manager.html
.. _Dish Manager Interface Description: https://developer.skao.int/projects/ska-mid-dish-manager/en/latest/api/devices/dish_manager_interface.html
