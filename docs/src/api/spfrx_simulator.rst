===============
SPFRx Simulator
===============

* `SPFRx Simulator Device Server`_
* `SPFRx Simulator Interface Description`_

.. _SPFRx Simulator Device Server: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/devices/SPFRX.html
.. _SPFRx Simulator Interface Description: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/interface/spfrx_spec.html
