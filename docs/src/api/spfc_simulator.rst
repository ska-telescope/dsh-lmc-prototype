==============
SPFC Simulator
==============

* `SPFC Simulator Device Server`_
* `SPFC Simulator Interface Description`_

.. _SPFC Simulator Device Server: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/devices/SPF.html
.. _SPFC Simulator Interface Description: https://developer.skao.int/projects/ska-mid-dish-simulators/en/latest/api/interface/spf_spec.html
