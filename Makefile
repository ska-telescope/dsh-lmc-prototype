#
# Project makefile for SKA Dish LMC. 
#

#############################
# BASE
#############################
SHELL=/bin/bash
.SHELLFLAGS=-o pipefail -c

NAME=ska-dish-lmc
VALUES_FILE ?= charts/ska-dish-lmc/values.yaml

VERSION=$(shell grep -e "^version = s*" pyproject.toml | cut -d = -f 2 | xargs)
IMAGE=$(CAR_OCI_REGISTRY_HOST)/$(NAME)
DOCKER_BUILD_CONTEXT=.
DOCKER_FILE_PATH=Dockerfile

MINIKUBE ?= true ## Minikube or not
SKA_TANGO_OPERATOR = true
TANGO_HOST ?= tango-databaseds:10000  ## TANGO_HOST connection to the Tango DS
CLUSTER_DOMAIN ?= cluster.local ## Domain used for naming Tango DS
K8S_TIMEOUT ?= 600s
KUBE_NAMESPACE=ci-$(CI_PROJECT_NAME)-$(CI_JOB_ID)

-include .make/base.mk

#############################
# XRAY
#############################
XRAY_TEST_RESULT_FILE = "build/cucumber.json"
XRAY_EXTRA_OPTS = "-v"
-include .make/xray.mk


#############################
# PYTHON
#############################
# set line length for all linters 
PYTHON_LINE_LENGTH = 99

# Set the specific environment variables required for pytest
PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=.:./src \
							TANGO_HOST=$(TANGO_HOST)

PYTHON_VARS_AFTER_PYTEST = -m '$(MARK)' --forked --json-report --json-report-file=build/report.json --junitxml=build/report.xml --cucumberjson=build/cucumber.json --event-storage-files-path="build/events" --ds-simulator-url="opc.tcp://ds-opcua-server-simulator-001-svc.${KUBE_NAMESPACE}:4840/dish-structure/server/" --event-and-log-storage-files-path="build/events_and_logs" --sequence-diagram-storage-files-path="build/sequence_diagrams"

# This variable is used for appending options to acceptance tests from the gitlab pipeline
# E.g. PIPELINE_TEST_VAR="--no-reset-between-tests" will allow acceptance tests to run without 
# resets between tests in the pipeline without making a code change
PIPELINE_TEST_VAR ?=

python-test: MARK = unit 
# specify the config file for python test
python-test: PYTHON_VARS_AFTER_PYTEST += -c pyproject.toml
k8s-test-runner: MARK = acceptance
k8s-test-runner: TANGO_HOST = tango-databaseds.$(KUBE_NAMESPACE).svc.$(CLUSTER_DOMAIN):10000
k8s-test-runner: PYTHON_VARS_AFTER_PYTEST += $(PIPELINE_TEST_VAR)

ifeq ($(CI_JOB_NAME_SLUG),k8s-smoke-test)
k8s-test-runner: MARK = smoke_test
endif

-include .make/python.mk

#############################
# OCI, K8s, Helm
#############################
OCI_TAG = $(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
CI_REGISTRY ?= registry.gitlab.com

# Use the previously built image when running in the pipeline
ifneq ($(CI_JOB_ID),)
K8S_TEST_IMAGE_TO_TEST=$(CI_REGISTRY)/ska-telescope/$(NAME)/$(NAME):$(OCI_TAG)
endif

CUSTOM_VALUES = --set dshlmc.image.image=$(NAME) \
	--set dshlmc.image.registry=$(CI_REGISTRY)/ska-telescope/$(NAME) \
	--set dshlmc.image.tag=$(OCI_TAG)

K8S_CHART_PARAMS ?= --set global.minikube=$(MINIKUBE) \
	--set global.operator=$(SKA_TANGO_OPERATOR) \
	--set global.tango_host=$(TANGO_HOST) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	$(CUSTOM_VALUES) \
	--values $(VALUES_FILE)

-include .make/oci.mk
-include .make/k8s.mk
-include .make/helm.mk

# include your own private variables for custom deployment configuration
-include PrivateRules.mak
