[tool.poetry]
name = "ska-dish-lmc"
version = "7.0.0"
description = "Mid Dish LMC Tango devices"
authors = ["Team Karoo"]
license = "BSD-3-Clause"

packages = [
    { include = "ska_dish_lmc", from = "src" }
]
include = [
    { path = 'tests'}
]

[tool.poetry.scripts]
DishLogger = "ska_dish_lmc.DishLogger:run"

[[tool.poetry.source]]
name = 'ska-nexus'
url = 'https://artefact.skao.int/repository/pypi-internal/simple'

[[tool.poetry.source]]
name = "PyPI-public"
url = 'https://pypi.org/simple'

[tool.poetry.dependencies]
python = "~3.10"
pytango = "9.5.0"
ska-tango-base = "1.0.0"
typing-extensions = "^4.3.0"

[tool.poetry.dev-dependencies]
black = "22.3.0"
pylint = "2.13.3"
pytest = "7.1.1"
pytest-cov = "3.0.0"
isort = "5.10.1"
flake8 = "4.0.1"
pylint-junit = "0.3.2"
coverage = "6.3.2"
pytest-json-report = "1.5.0"
ipython = "7.21.0"
pytest-forked = "^1.4.0"
pytest-bdd = "^6.1.1"
asyncua = "^1.0.2"
astropy = "^6.0.0"
pyhdbpp = "^1.1.5"
psycopg2-binary = "^2.9.9"
jsonschema = "4.20.0"
nbqa = "1.8.5"

[tool.poetry.group.docs]
optional = true

[tool.poetry.group.docs.dependencies]
docutils = "< 0.20"
Sphinx = "^5.3.0"
ska-ser-sphinx-theme = "^0.1.2"
sphinx-copybutton = "*"
sphinx-tabs = "*"
sphinx-autodoc-typehints = "*"
sphinxcontrib-plantuml = "*"
typing_extensions = "*"

[tool.pytest.ini_options]
bdd_features_base_dir = "tests/features"
markers = [
    "unit",
    "smoke_test",
    "lmc_logger",
    "verify_eda",
    "acceptance",
    "XTP-813",
    "VTS-226",
    "XTP-811",
    "XTP-3090",
    "XTP-5414",
    "XTP-5703",
    "XTP-6269",
    "XTP-6270",
    "XTP-6271",
    "XTP-6439",
    "XTP-5773",
    "XTP-3392",
    "XTP-3310",
    "XTP-14050",
    "XTP-15464",
    "XTP-15465",
    "XTP-15466",
    "XTP-15467",
    "XTP-15468",
    "XTP-15469",
    "XTP-15470",
    "XTP-15471",
    "XTP-28438",
    "XTP-28120",
    "XTP-16286",
    "XTP-13924",
    "XTP-28119",
    "XTP-37884",
    "XTP-39181",
    "XTP-39717",
    "L2-4699",
    "L2-4700",
    "L2-4697",
    "L2-4698",
    "L2-4621",
    "L2-5125",
    "L2-5128",
    "L2-5121",
    "L2-4677",
    "L2-4612",
    "L2-4627",
    "L2-4588",
    "L2-4688",
    "L2-4636",
    "L2-4635",
    "L2-4649",
    "L2-4598",
    "L2-4600",
    "L2-4634",
    "L2-4704",
    "L2-4708",
    "L2-4692",
    "L2-4696",
    "L2-4651",
    "L2-4695",
    "L2-4691",
    "L2-4633",
    "L2-4639",
    "L2-4630",
    "L2-4624",
    "L2-4632",
]

[tool.isort]
multi_line_output = 3
include_trailing_comma = true
force_grid_wrap = 0
line_length = 99

[tool.black]
line-length = 99

[tool.flake8]
max-line-length = 99

[tool.coverage.paths]
source = ["src"]

[tool.coverage.run]
data_file = ".coverage/.coverage"
branch = true
source = ["ska_dish_lmc"]

[tool.coverage.report]
show_missing = false

[tool.pylint.messages_control]
disable = [
  "fixme",
  "duplicate-code",
  "logging-fstring-interpolation",
  "invalid-name",
  "superfluous-parens",
  "unused-argument",
  "redefined-outer-name",
  "too-many-arguments"
]

[build-system]
requires = ["poetry-core>=1.0.0"]
build-backend = "poetry.core.masonry.api"

[tool.nbqa.addopts]
flake8 = [
    "--extend-ignore=E402"
]
pylint = [
    "--ignore-paths=notebooks,docs"
]
