[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-dish-lmc/badge/?version=latest)](https://developer.skao.int/projects/ska-dish-lmc/en/latest/?badge=latest)

## **Dish LMC Software Overview**  
Dish LMC is the monitoring and control (M&C) system for the SKA MID Dish of the Square Kilometer Array (SKA).

It is built using Python and utilizes the Tango Controls toolkit.
The system consists of three main components, namely:
* Dish Manager (formerly known as DishMaster)
  * [DishManager](https://gitlab.com/ska-telescope/ska-mid-dish-manager/-/tree/main/)
* Dish Structure (DS) Manager
  * [Dish Structure (DS) Manager](https://gitlab.com/ska-telescope/ska-mid-dish-ds-manager)
* Dish Logger
  * [Dish Logger](https://gitlab.com/ska-telescope/ska-dish-lmc/-/tree/master/src/ska_dish_lmc)

To aid in testing we also have our own simulators.

* SPF Controller Simulator and SPFRx Controller Simulator
  * [SPF Controller Simulator & SPFRx Controller Simulator](https://gitlab.com/ska-telescope/ska-mid-dish-simulators)

## Dish LMC Device context

The main entrypoint for TMC to control and monitor a dish is the DishManager device.

![Devices](./docs/src/images/DishLMC.png)

## Repository layouts

### Devices for full deployment

![CI/CD](./docs/src/images/full-deployment.png)

### Devices for CI/CD

![CI/CD](./docs/src/images/cicd-deployment.png)

### Additional deployments

Additional deployments can be found here:
[Deployments](https://miro.com/app/board/o9J_lneOGik=/?moveToWidget=3458764558043126213&cot=14)


### Deployment
```bash
$ helm upgrade --install dev . -n ska-dish-lmc \
	--set global.exposeDatabaseDS=true \
	--set global.dishes="{001}" \
	--set global.minikube=true \
	--set global.operator=true \
	--set ska-mid-dish-simulators.enabled=true \
	--set ska-mid-dish-simulators.deviceServers.spfdevice.enabled=true \
	--set ska-mid-dish-simulators.deviceServers.spfrxdevice.enabled=true \
	--set ska-mid-dish-simulators.dsOpcuaSimulator.enabled=true \
	--set ska-mid-dish-b5dc-proxy.enabled=true \
	--set ska-mid-dish-dcp-lib.b5dcSimulator.enabled=true
```

ska-tango-base is not deployed by default, to deploy it add the 
--set below:
```bash
  --set ska-tango-base.enabled=true
```


### --set flag options
| Option | Description | Example values |
|--------|-------------|----------------|
| global.dishes | Determine the number of sets of DISH.LMC devices. <br/><br/> <i>Note: The values determine the dish IDs</i> | {001}, {001, 002} |
| global.minikube | Set to <i>true</i> when deploying to minikube | true, false |
| global.operator | Set to <i>true</i> to use ska tango operator for k8s deployment | true, false |
| ska-mid-dish-manager.dishmanager.&lt;device&gt;.fqdn | Full path | tango://127.0.0.1:45678/foo/bar/1#dbase=no |
| ska-mid-dish-manager.dishmanager.&lt;device&gt;.family_name | The middle value in the tango triplet <br/><br/> <i>Note: The device name has to conform to [ADR-9](https://confluence.skatelescope.org/display/SWSI/ADR-9+Update+naming+conventions+for+TANGO+Devices+and+Servers) e.g. mid-dish/simulator-spfrx/SKA001</i> | <ul><li>simulator-spfc</li><li>simulator-spfrx</li><li>spfc</li><li>spfrxpu-controller</li></ul> |
| ska-mid-dish-manager.ska-mid-dish-ds-manager.dishstructuremanager.dsc.fqdns | List containing a Dish Structure Controller device FQDN for each instance of DISH.LMC to connect to. <br/><br/> <i>Note: The number of FQDNs specified must match the number of Dish IDs present in global.dishes</i> | {opc.tcp://127.0.0.1:1234/OPCUA/SimulationServer, opc.tcp://127.0.0.1:5678/OPCUA/SimulationServer} |
| ska-mid-dish-simulators.enabled | Enable or disable the device simulators chart in dish-simulators repository| true, false |
| ska-mid-dish-simulators.deviceServers.spfdevice.enabled | Enable or disable the SPF device simulator | true, false |
| ska-mid-dish-simulators.deviceServers.spfrxdevice.enabled | Enable or disable the SPFRx device simulator | true, false |
| ska-mid-dish-simulators.dsOpcuaSimulator.enabled | Enable or disable the OPCUA server | true, false |
| ska-tango-base.enabled | Enable or disable the ska-tango-base dependency | true, false |
| ska-mid-dish-b5dc-proxy.enabled | Enable or disable the Band 5 downconverter proxy device | true, false |
| ska-mid-dish-dcp-lib.b5dcSimulator.enabled | Enable or disable the Band 5 downconverter simulator | true, false|

## Deployments

In the present repository it is possible to deploy the chart in different clusters. In specific it is possible to deploy in stfc-techops, psi-mid, and itf-mid clusters with 6 different stages which are: 'deploy-to-mid-psi', 'deploy-to-stfc-techops', 'deploy-lmc-to-itf-spf', 'deploy-lmc-to-itf-karoo-sims', 'deploy-lmc-to-itf-spfrx' and 'deploy-lmc-to-itf-ds'. 
The last 4 stages will deploy into the same cluster (itf-mid) but in 4 different k8s namespaces. 

For each stage, there are 12 jobs available which can create the ska-dish-lmc environment. There is an additional job in the 'deploy-lmc-to-itf-spfrx' stage to register the pyhsical SPFRx device in the ITF on the tango database. For example in the stfc-techops stage, there are: 

| Job | Description |
|-----|-------------|
| create-namespace-stfc-techops | Creates the namespace used by the rest of the jobs |
| deploy-ska-tango-base-stfc-techops | Deploys the ska-tango-base chart |
| deploy-ska-dish-lmc-stfc-techops | Deploys the Dish LMC devices |
| deploy-taranta-stfc-techops | Deploys Taranta charts (taranta and tangogql) |
| gather-deployment-data-stfc-techops | Gathers info about pods and containers running in the namespace |
| smoke-test-stfc-techops | Verify that LMC devices and the tango db are available |
| integration-test-stfc-techops | Run the set of integration tests |
| output-logs-stfc-techops | Acquire & output dish logger logs from the Dish LMC deployment in MID PSI cluster |
| delete-namespace-stfc-techops | Deletes the namespace (the entire environment) |
| delete-ska-dish-lmc-stfc-techops | Deletes the Dish LMC devices deployment |
| delete-taranta-stfc-techops | Delete the Taranta charts |
| delete-ska-tango-base-stfc-techops | Delete the ska-tango-base chart |

Please note that: 
- the jobs in the various stages differ only for the suffix (i.e. stfc-techops vs mid-psi).
- the available environments will be shown as [gitlab environments](https://gitlab.com/ska-telescope/ska-dish-lmc/-/environments).

The definition of the deployments are available in the folder gitlab-ci/includes where, for each cluster, for each environment the values file applied is available. For examples the values file applied to the psi-mid is available in the folder: gitlab-ci/includes/psi-mid/mid-psi-ska-dish-lmc/values.yaml. 


### PSI MID specific

The deployment of the dish lmc will exclude the SPFRx Simulator.

### ITF specific

There are 4 different deployments targetting the ITF, shown in the table below:

| Stage | Deployment details | Cluster namespace | Taranta link |
|-------|--------------------|-------------------|--------------|
| deploy-lmc-to-itf-ds | DishManager + Dish Structure Simulator (PLC) | miditf-lmc-002-ds | https://k8s.miditf.internal.skao.int/miditf-lmc-002-ds/taranta/devices |
| deploy-lmc-to-itf-karoo-sims | DishManager + Karoo software simulators | miditf-lmc-003-karoo-sims | https://k8s.miditf.internal.skao.int/miditf-lmc-003-karoo-sims/taranta/devices |
| deploy-lmc-to-itf-spf | DishManager + Physical SPF + Karoo software simulators | miditf-lmc-004-spf | https://k8s.miditf.internal.skao.int/miditf-lmc-004-spf/taranta/devices |
| deploy-lmc-to-itf-spfrx | DishManager + Physical SPFRx + Karoo software simulators | miditf-lmc-005-spfrx | https://k8s.miditf.internal.skao.int/miditf-lmc-005-spfrx/taranta/devices |

#### Binderhub

Binderhub is available to all deployments in the Mid ITF. Firstly, connect to the Mid ITF VPN where binderhub will then be accessible by going to the following url: https://k8s.miditf.internal.skao.int/binderhub/.

A JupyTango notebook can be created by building and launching the JupyTango gitlab repository (https://gitlab.com/tango-controls/jupyTango). The following details can then be used to access the tango database.

- TANGO_HOST={DATABASEDS_NAME}.{KUBE_NAMESPACE}.svc.{CLUSTER_DOMAIN}:10000

For more information on Mid ITF environment variables see [this page](https://confluence.skatelescope.org/display/SE/Mid+ITF+Notebook+Environment+Variables).

#### Taranta

Taranta is deployed in each of the Mid ITF deployments and is accessible through the following url after substituting in the chosen deployment namespace.
- https://k8s.miditf.internal.skao.int/INSERT_NAMESPACE/taranta/devices/

## Pytest
### Device FQDN arguments
The following arguments can be used with the pytest command to set the tango device FQDNs for the subservient devices.

<i>Note: These can be added to the PYTHON_VARS_AFTER_PYTEST variable in the makefile to set the FQDNs during pipeline runs.<i>
| Device | Flag |
|--------|-------------|
| Dish Manager | --dish-manager-fqdn |
| Dish Structure | --dish-structure-fqdn |
| SPF | --spf-fqdn |
| SPFRx | --spfrx-fqdn |

### Example
```bash
$ pytest -m "acceptance" tests/ --dish-structure-fqdn=some/test/fqdn
```

## Notebooks
A set of Jupyter notebooks are made available in tutorial-like format to demonstrate the basic usage and functionality of Dish LMC.
These notebooks together with setup instructions can be found in the [notebooks folder](https://gitlab.com/ska-telescope/ska-dish-lmc/-/tree/master/notebooks).

