Taranta DashBoards
==================

# Instructions

- Download the dashboards from the folders here as needed

![Dashboards](./screenshots/Dashboards.png)

-  Upload it to Taranta

![Import](./screenshots/Import.png)

-  Configure the dashboard by selecting the cog

![Config](./screenshots/Config.png)

-  Add the required variables

![Variables](./screenshots/Vars.png)

-  Run the dashboard

# Dashboards

## Dish

![Dish](./screenshots/Dish_Manager.png)

## SPFRx

![SPFRx](./screenshots/SPFRX_dashboard.png)

## SPFC

![SPFC](./screenshots/Band_1.png)

## DS Manager

![DS Manager](./screenshots/DS_Dashboard.png)

## Band 5 Downconverter proxy

![Band 5 Downconverter proxy](./screenshots/B5dc_dashboard.png)
