
LOCAL_DEPLOYMENT = --set global.dishes="{001}" \
	--set global.exposeDatabaseDS=true \
	--set global.minikube=$(MINIKUBE) \
	--set ska-mid-dish-simulators.enabled=true \
	--set ska-mid-dish-simulators.dsOpcuaSimulator.enabled=true \
	--set ska-mid-dish-simulators.deviceServers.spfdevice.enabled=true \
	--set ska-mid-dish-simulators.deviceServers.spfrxdevice.enabled=true \
	--set ska-tango-base.enabled=true

ifeq ($(GITLAB_CI),false)
K8S_CHART_PARAMS = $(LOCAL_DEPLOYMENT)
endif
