"""Module containing Dish LMC buildState data class."""
import dataclasses


@dataclasses.dataclass
class DishLMCBuildStateDataclass:
    """Dataclass to format Dish LMC buildState data."""

    package: str
    release_version: str
