# pylint: disable=missing-module-docstring,missing-class-docstring,missing-function-docstring
# pylint: disable=abstract-method,too-few-public-methods,too-many-locals,use-maxsplit-arg
# pylint: disable=no-self-use
import ast
import re

# Regex patterns
INCOMING_COMMAND_CALL_REGEX_PATTERN = r"-> (\w+\.\w+)\(\)"
RETURN_COMMAND_CALL_REGEX_PATTERN = r"^(.*) <- (\w+\.\w+)\(\)"
LRC_RETURN_VAL_REGEX_PATTERN = r"\(\[(.*)\], \['(.*)'\]\)"
LRC_TUPLE_REGEX_PATTERN = r"'([0-9a-zA-Z._]*)', '([^']*)'"
LOG_REGEX_PATTERN = r"([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|([^|]*)\|(.*)"
EVENT_REGEX_PATTERN = r"([^\t]*)\t([^\t]*)\t([^\t]*)\t(.*)"


class PlantUMLSequenceDiagram:
    def __init__(self):
        self.diagram_code = ""

    def start_diagram(self, title, actor):
        self.diagram_code = "@startuml SequenceDiagram\n"
        self.diagram_code += f"title {title}\n"
        self.diagram_code += f"actor {actor}\n"

    def add_participant(self, participant):
        self.diagram_code += f"participant {self.clean_text(participant)}\n"

    def end_diagram(self):
        self.diagram_code += "@enduml"

    def clean_text(self, text):
        return text.replace("-", "_")

    def wrap_text(self, text, max_width=50, max_length=500):
        if max_width <= 0:
            raise ValueError("max_width must be a positive integer.")

        if len(text) > max_length:
            truncated_length = max_length - 3
            text = text[:truncated_length] + "..."

        lines = []
        for i in range(0, len(text), max_width):
            end_index = i + max_width
            segment = text[i:end_index]
            lines.append(segment)

        result = "\n".join(lines).encode("unicode_escape").decode("utf-8")
        return result

    def wrap_text_on_spaces(self, text, max_width=50):
        if max_width <= 0:
            raise ValueError("max_width must be a positive integer.")

        words = text.split(" ")
        line = ""
        lines = []

        for word in words:
            if len(line) + len(word) < max_width:
                line += " " + word
            else:
                lines.append(line)
                line = word
        lines.append(line)

        result = "\n".join(lines).encode("unicode_escape").decode("utf-8")
        return result

    def add_note_over(self, device, note, color="lightgreen"):
        device = self.clean_text(device)
        # note = self.wrap_text(note)

        self.diagram_code += f"rnote over {device} #{color}: {note}\n"

    def add_hexagon_note_over(self, device, note, color="lightgrey"):
        device = self.clean_text(device)
        # note = self.wrap_text(note)

        self.diagram_code += f"hnote over {device} #{color}: {note}\n"

    def add_command_call(self, from_device, to_device, note):
        from_device = self.clean_text(from_device)
        to_device = self.clean_text(to_device)
        note = self.wrap_text(note)

        self.diagram_code += f"{from_device} -> {to_device}: {note}\n"

    def add_command_response(self, from_device, to_device, note):
        from_device = self.clean_text(from_device)
        to_device = self.clean_text(to_device)
        note = self.wrap_text(note)

        self.diagram_code += f"{from_device} --> {to_device}: {note}\n"


class LogParser:
    def __init__(self):
        self.log_pattern_callbacks: list[tuple[str, callable]] = []

    def _parse_log_line(self, log_line):
        for pattern, pattern_cb in self.log_pattern_callbacks:
            match = re.search(pattern, log_line)
            if match:
                group_values = match.groups()
                pattern_cb(*group_values)
                break

    def parse_file(self, file_path):
        with open(file_path, "r", encoding="utf-8") as file:
            logs = file.readlines()

        for log in logs:
            self._parse_log_line(log)


class DishLMCEventsAndLogsFileParser(LogParser):
    def __init__(self, show_events=False, show_component_state_updates=False):
        super().__init__()

        self.show_events = show_events
        self.show_component_state_updates = show_component_state_updates

        self.sequence_diagram = PlantUMLSequenceDiagram()
        self.device_hierarchy = ["pytest", "dish-manager", "ds-manager"]
        self.test_started = False
        self.running_lrc_status_updates = {}

        self.log_pattern_callbacks = [
            [LOG_REGEX_PATTERN, self.log_callback],
            [EVENT_REGEX_PATTERN, self.event_callback],
            [r"Events set up, test starting", self.test_started_callback],
        ]

    def get_likely_caller_from_hierarchy(self, device) -> str:
        if device not in self.device_hierarchy or device == self.device_hierarchy[0]:
            return "unknown"
        device_index = self.device_hierarchy.index(device)
        return self.device_hierarchy[device_index - 1]

    def get_method_from_lrc_id(self, lrc_id) -> str:
        return "_".join(lrc_id.split("_")[2:])

    def parse(self, file_path, output_file_path, actor="pytest"):
        log_file_name = file_path.split("/")[-1]

        cleaned_log_file_name = self.sequence_diagram.clean_text(log_file_name)
        title = f"Sequence diagram generated from\n{cleaned_log_file_name}".encode(
            "unicode_escape"
        ).decode("utf-8")

        self.test_started = False
        self.running_lrc_status_updates = {}
        self.sequence_diagram.start_diagram(title, actor)

        # Add participants to ensure order of swimlanes
        for device in self.device_hierarchy[1:]:
            self.sequence_diagram.add_participant(device)

        self.parse_file(file_path)

        self.sequence_diagram.end_diagram()

        # Save the PlantUML diagram code to a file
        with open(output_file_path, "w", encoding="utf-8") as f:
            f.write(self.sequence_diagram.diagram_code)

    def test_started_callback(self):
        self.test_started = True

    def log_callback(
        self, prefix, iso_date_string, log_level, runner, action, log_line, device, message
    ):
        # 1724676115.079 -  Log  - 1|2024-08-26T12:41:55.079Z|DEBUG|Thread-9 (_event_consumer)|
        # _component_state_changed|dish_manager_cm.py#390|
        # tango-device:mid-dish/dish-manager/SKA001|...
        if not self.test_started:
            return

        device = device.split("/")[1]  # dish-manager

        if action == "_info_patch":
            self.info_patch_cb(
                prefix, iso_date_string, log_level, runner, action, log_line, device, message
            )
        elif action == "_update_component_state" and self.show_component_state_updates:
            self.component_state_update_cb(
                prefix, iso_date_string, log_level, runner, action, log_line, device, message
            )

    def info_patch_cb(
        self, prefix, iso_date_string, log_level, runner, action, log_line, device, message
    ):
        if "->" in message:
            match = re.search(INCOMING_COMMAND_CALL_REGEX_PATTERN, message)
            if match:
                method = match.group(1)
                method = method.split(".")[1]
                caller = self.get_likely_caller_from_hierarchy(device)
                self.sequence_diagram.add_command_call(caller, device, f'""{method}""')

        elif "<-" in message:
            match = re.search(RETURN_COMMAND_CALL_REGEX_PATTERN, message)
            if match:
                return_val = match.group(1)
                method = match.group(2)
                method = method.split(".")[1]
                caller = self.get_likely_caller_from_hierarchy(device)

                self.sequence_diagram.add_command_response(
                    device, caller, f'""{method}"" -> {return_val}'
                )

    def component_state_update_cb(
        self, prefix, iso_date_string, log_level, runner, action, log_line, device, message
    ):
        search_string = r"Updating (\w*) (\w*) component state with \[(.*)\]"

        match = re.search(search_string, message)

        if match:
            string_dict = match.group(3)
            string_dict = string_dict.replace("<", "'<")
            string_dict = string_dict.replace(">", ">'")
            component_state_updates = ast.literal_eval(string_dict)

            note_text = "Component state update"
            for attr, attr_value in component_state_updates.items():
                note_text += f'\n""{attr} = {attr_value}""'.encode("unicode_escape").decode(
                    "utf-8"
                )
            self.sequence_diagram.add_hexagon_note_over(device, note_text)

    def event_callback(self, prefix, device, event_attr, val):
        # 1724660914.761 - Event - 2024-08-26 08:28:34.761448	DishManager(mid-dish/dish-manager
        # /ska001)	longrunningcommandstatus	('1724660914.663982_241979260268973_SetStowMode',
        # 'COMPLETED')
        if not self.test_started:
            return

        device = device.split("/")[1]  # dish-manager
        caller = self.get_likely_caller_from_hierarchy(device)

        if "longrunningcommand" in event_attr:
            self.handle_lrc_event_log(device, caller, event_attr, val)
        elif self.show_events:
            self.sequence_diagram.add_note_over(
                device,
                f'Event\n""{event_attr} = {val.strip()}""'.encode("unicode_escape").decode(
                    "utf-8"
                ),
            )

    def handle_lrc_event_log(self, device, caller, event_attr, val):
        if event_attr == "longrunningcommandstatus":
            lrc_statuses = re.findall(LRC_TUPLE_REGEX_PATTERN, val)
            for index, (lrc_id, status) in enumerate(lrc_statuses):
                # If there are any newer updates for this lrc in the LRC statuses then skip this
                newer_status_found = False
                if index + 1 < len(lrc_statuses):
                    for i in range(index + 1, len(lrc_statuses)):
                        if lrc_statuses[i][0] == lrc_id:
                            newer_status_found = True
                            break

                if newer_status_found:
                    break

                method_name = self.get_method_from_lrc_id(lrc_id)

                if status == "STAGING":
                    # Only track methods which are called in the scope of the file
                    # This avoids some noise left over in LRC attributes from previous test / setup
                    self.running_lrc_status_updates[lrc_id] = []

                # Only update if its a method called in the scope of this file and its a new status
                if (
                    lrc_id in self.running_lrc_status_updates
                    and status not in self.running_lrc_status_updates[lrc_id]
                ):
                    self.running_lrc_status_updates[lrc_id].append(status)
                    self.sequence_diagram.add_command_response(
                        device, caller, f'""{method_name}"" -> {status}'
                    )
        elif event_attr == "longrunningcommandprogress":
            lrc_progresses = re.findall(LRC_TUPLE_REGEX_PATTERN, val)
            for lrc_id, progress in lrc_progresses:
                # Only show progress updates for methods which have been staged
                if lrc_id in self.running_lrc_status_updates:
                    method_name = self.get_method_from_lrc_id(lrc_id)
                    self.sequence_diagram.add_command_call(
                        device, device, f'""{method_name}"" -> {progress}'
                    )
        elif event_attr == "longrunningcommandresult":
            pass


if __name__ == "__main__":
    log_file = (
        "events_and_logs_test_lp_to_stow_mode_transition"
        "[STOW-SetStandbyLPMode-STANDBY-LP-STANDBY-LP-LOW-POWER-STANDBY-LP-LOW-POWER-STANDBY]"
        ".txt"
    )
    log_file_name_no_extension = log_file.split(".")[0]
    output_file = f"sequence_diagram_{log_file_name_no_extension}.puml"

    file_parser = DishLMCEventsAndLogsFileParser(
        show_events=True, show_component_state_updates=True
    )
    file_parser.parse(log_file, output_file, actor="pytest")
