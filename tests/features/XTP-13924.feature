@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-13924 @XTP-811 @XTP-16286 @L2-4633 @L2-4691
    Scenario: Test Dish LMC Dish Startup (LP Mode)
        Given dish_structure operatingMode reports STANDBY-LP
        And spf operatingMode reports STANDBY-LP
        And spfrx operatingMode reports STANDBY
        Then dish_manager dishMode reports STANDBY-LP
