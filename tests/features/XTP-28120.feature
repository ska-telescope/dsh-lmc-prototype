@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-28120 @XTP-811 @XTP-16286 @L2-5121 @L2-4704
    Scenario Outline: Dish LMC is able to update pointing model parameters regardless of configuredBand
        Given dish_manager and dish_structure have attributes for setting pointing model parameters
        When I write <value> to <pointing_model_parameter>
        Then the dish_structure <pointing_model_parameter> is updated
        And dish_manager <pointing_model_parameter> should report <expected_value>

            Examples:
                | pointing_model_parameter | value | expected_value |
                | band1PointingModelParams | [1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10, 11.11, 12.12, 13.13, 14.14, 15.15, 16.16, 17.17, 18.18] | [1.1, 2.2, 3.3, 4.4, 5.5, 6.6, 7.7, 8.8, 9.9, 10.10, 11.11, 12.12, 13.13, 14.14, 15.15, 16.16, 17.17, 18.18] |
                | band2PointingModelParams | [2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7, 9.8, 10.9, 11.10, 12.11, 13.12, 14.13, 15.14, 16.15, 17.16, 18.17, 19.18] | [2.1, 3.2, 4.3, 5.4, 6.5, 7.6, 8.7, 9.8, 10.9, 11.10, 12.11, 13.12, 14.13, 15.14, 16.15, 17.16, 18.17, 19.18] |
                | band3PointingModelParams | [3.1, 4.2, 5.3, 6.4, 7.5, 8.6, 9.7, 10.8, 11.9, 12.10, 13.11, 14.12, 15.13, 16.14, 17.15, 18.16, 19.17, 20.18] | [3.1, 4.2, 5.3, 6.4, 7.5, 8.6, 9.7, 10.8, 11.9, 12.10, 13.11, 14.12, 15.13, 16.14, 17.15, 18.16, 19.17, 20.18] |
                | band4PointingModelParams | [4.1, 5.2, 6.3, 7.4, 8.5, 9.6, 10.7, 11.8, 12.9, 13.10, 14.11, 15.12, 16.13, 17.14, 18.15, 19.16, 20.17, 21.18] | [4.1, 5.2, 6.3, 7.4, 8.5, 9.6, 10.7, 11.8, 12.9, 13.10, 14.11, 15.12, 16.13, 17.14, 18.15, 19.16, 20.17, 21.18] |
