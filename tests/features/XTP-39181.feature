@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-39181 @XTP-811 @XTP-16286 @L2-4677
    Scenario: Test Dish LMC Reporting Models
        Given dish_manager establishes connection with dish_structure, spf and spfrx
        When dish_manager receives a STANDBY-FP mode request
        Then dish_manager reports that the command has been accepted
        And dish_manager pushes change events on the dishMode attribute
