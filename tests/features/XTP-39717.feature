@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-39717 @XTP-811 @XTP-16286 @L2-4598
    Scenario: Dish LMC executes STOW to DS within 1s of issue from TMC
        Given dish_manager receives a STOW request from a client
        Then dish_structure should execute STOW within 1 second of receiving the command
