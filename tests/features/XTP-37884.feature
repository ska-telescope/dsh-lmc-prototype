@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-37884 @XTP-811 @XTP-16286 @L2-4633 @L2-4691
    Scenario: Test Dish LMC Dish Startup (STARTUP Mode)
        Given either dish_structure, spf or spfrx operatingMode reports STARTUP
        Then dish_manager dishMode reports STARTUP
