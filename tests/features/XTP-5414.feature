@VTS-226
Feature: Dish LMC acceptance tests

    @XTP-5414 @XTP-811 @XTP-16286 @L2-4708 @L2-4692 @L2-4704 @L2-4639
    Scenario: Test dish Track command
        Given dish_manager dishMode reports OPERATE
        And dish_manager pointingState reports READY
        And dish_manager receives pointing updates on the programTrackTable
        When I issue Track on dish_manager
        Then dish_manager should slew to the commanded position
        And dish_manager pointingState should transition to TRACK on target
        And dish_manager achievedTargetLock should transition to True
        And the difference between actual and desired position should be less than or equal to the configured threshold

        @XTP-5414 @XTP-811 @XTP-16286 @L2-4708 @L2-4692 @L2-4704
        Scenario: Test Dish TrackStop command
            Given dish_manager dishMode reports OPERATE
            And dish_manager pointingState reports TRACK or SLEW
            And dish_structure pointingState reports TRACK or SLEW
            When I issue TrackStop on dish_manager
            Then dish_manager pointingState should transition to READY
            And dish_structure pointingState should transition to READY
