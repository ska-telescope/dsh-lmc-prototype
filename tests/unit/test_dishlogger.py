"""Unit tests checking DishLogger behaviour."""

import logging

import pytest
from tango.test_context import DeviceTestContext

from ska_dish_lmc.DishLogger import DishLogger, LogComponentManager

_TEST_LOG = [
    "1650964795495",
    "ERROR",
    "mid-dish/dish-manager/SKA001",
    "TangoUtils::DeviceAttributeToCorbaAny() - A Message",
    "",
    "@7f48dcc80700 [7]",
]


@pytest.mark.unit
def test_log_command(caplog):
    """Function to test log command for DishLogger device"""
    with DeviceTestContext(DishLogger, process=True) as proxy:
        with caplog.at_level(logging.INFO):
            proxy.Log(_TEST_LOG)


@pytest.mark.unit
def test_log_format(caplog):
    """Test for checking the log format"""
    log_c = LogComponentManager(logger=logging.getLogger())
    log_c.log(
        timestamp=_TEST_LOG[0],
        tango_log_level=_TEST_LOG[1],
        tango_device=_TEST_LOG[2],
        message=_TEST_LOG[3],
    )
    assert caplog.records
    log_record = caplog.records[0]

    assert log_record.lineno == 0
    assert log_record.message == "A Message"
    assert log_record.created == 1650964795.495
    assert log_record.filename == "unknown_file"
    assert log_record.funcName == "TangoUtils::DeviceAttributeToCorbaAny()"
    assert log_record.levelname == "ERROR"
    assert log_record.threadName == "unknown_thread"
