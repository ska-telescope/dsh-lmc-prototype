# pylint:disable=inconsistent-return-statements
"""General utils for test devices."""
import queue
import time
from threading import Event
from typing import Any, List, Tuple

import numpy as np
import tango
from astropy.time import Time
from tango import CmdArgType

SKA_EPOCH = "1999-12-31T23:59:28Z"


def retrieve_attr_value(dev_proxy, attr_name):
    """Get the attribute reading from device"""
    current_val = dev_proxy.read_attribute(attr_name)
    if current_val.type == CmdArgType.DevEnum:
        current_val = getattr(dev_proxy, attr_name).name
    elif current_val.type == CmdArgType.DevState:
        current_val = str(current_val.value)
    else:
        current_val = current_val.value
    return current_val


def tango_dev_proxy(device_name, logger):
    """Provision a device proxy to a tango device server"""
    retry_time = 0.5  # seconds
    retry = 0
    max_retries = 3
    retry_event = Event()

    while retry <= max_retries and not retry_event.is_set():
        try:
            dp = tango.DeviceProxy(device_name)
        except tango.DevFailed as ex:
            reasons = {dev_error.reason for dev_error in ex.args}
            if "API_CantConnectToDatabase" in reasons:
                retry += 1
                logger.debug(
                    f"Failed to connect to database after {retry + 1} tries. "
                    f"Retrying {device_name} device server connection"
                )
                retry_event.wait(retry_time)
            else:
                logger.error(f" {device_name} device server connection failed. Reasons: {reasons}")
                raise
        else:
            logger.debug(f"{device_name} device server connection established")
            return dp
    raise Exception(f"Connection to the {device_name} device server failed")


def get_tai_from_unix_s(unix_s: float) -> float:
    """
    Calculate atomic time in seconds from unix time in seconds.

    :param unix_s: Unix time in seconds

    :return: atomic time (tai) in seconds
    """
    astropy_time_utc = Time(unix_s, format="unix")
    return astropy_time_utc.unix_tai


class EventStore:
    """Store events with useful functionality"""

    def __init__(self) -> None:
        self._queue = queue.Queue()

    def push_event(self, event: tango.EventData):
        """Store the event

        :param event: Tango event
        :type event: tango.EventData
        """
        self._queue.put(event)

    def wait_for_value(self, value: Any, timeout: int = 3):
        """Wait for a value to arrive

        Wait `timeout` seconds for each fetch.

        :param value: The value to check for
        :type value: Any
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If None are found
        :return: True if found
        :rtype: bool
        """

        try:
            events = []
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if not event.attr_value:
                    continue

                if isinstance(event.attr_value.value, np.ndarray):
                    if (event.attr_value.value == value).all():
                        return True
                    if np.isclose(event.attr_value.value, value).all():
                        return True
                    continue

                if event.attr_value.value != value:
                    continue
                if event.attr_value.value == value:
                    return True
        except queue.Empty as err:
            ev_vals = self.extract_event_values(events)
            raise RuntimeError(f"Never got an event with value [{value}] got [{ev_vals}]") from err

    # pylint:disable=inconsistent-return-statements
    def wait_for_command_result(self, command_id: str, command_result: Any, timeout: int = 5):
        """Wait for a long running command result

        Wait `timeout` seconds for each fetch.

        :param command_id: The long running command ID
        :type command_id: str
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: str
        """
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                if not event.attr_value:
                    continue
                if not isinstance(event.attr_value.value, tuple):
                    continue
                if len(event.attr_value.value) != 2:
                    continue
                (lrc_id, lrc_result) = event.attr_value.value
                if command_id == lrc_id and command_result == lrc_result:
                    return True
        except queue.Empty as err:
            raise RuntimeError(f"Never got an LRC result from command [{command_id}]") from err

    def wait_for_progress_update(self, progress_message: str, timeout: int = 5):
        """Wait for a long running command progress update

        Wait `timeout` seconds for each fetch.

        :param progress_message: The progress message to wait for
        :type progress_message: str
        :param timeout: the get timeout, defaults to 3
        :type timeout: int, optional
        :raises RuntimeError: If none are found
        :return: The result of the long running command
        :rtype: str
        """
        events = []
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                events.append(event)
                if not event.attr_value:
                    continue
                if not isinstance(event.attr_value.value, tuple):
                    continue
                progress_update = str(event.attr_value.value)
                if (
                    progress_message in progress_update
                    and event.attr_value.name == "longrunningcommandprogress"
                ):
                    return events
        except queue.Empty as err:
            event_info = [(event.attr_value.name, event.attr_value.value) for event in events]
            raise RuntimeError(
                f"Never got a progress update with [{progress_message}],",
                f" but got [{event_info}]",
            ) from err

    def clear_queue(self):
        """Clear out the queue."""
        while not self._queue.empty():
            self._queue.get()

    def get_queue_events(self, timeout: int = 3):
        """Get all the events out of the queue."""
        items = []
        try:
            while True:
                items.append(self._queue.get(timeout=timeout))
        except queue.Empty:
            return items

    @classmethod
    def extract_event_values(cls, events: List[tango.EventData]) -> List[Tuple]:
        """Get the values out of events

        :param events: List of events
        :type events: List[tango.EventData]
        :return: List of value tuples
        :rtype: List[Tuple]
        """
        event_info = [
            (event.attr_value.name, event.attr_value.value, event.device) for event in events
        ]
        return event_info

    def get_queue_values(self, timeout: int = 3):
        """Get the values from the queue"""
        items = []
        try:
            while True:
                event = self._queue.get(timeout=timeout)
                items.append((event.attr_value.name, event.attr_value.value))
        except queue.Empty:
            return items

    @classmethod
    def get_data_from_events(cls, events: List[tango.EventData]) -> List[Tuple]:
        """Retrieve the event info from the events

        :param events: list of
        :type events: List[tango.EventData]
        """
        return [(event.attr_value.name, event.attr_value.value) for event in events]


def get_tai_timestamp_from_unix_s(unix_s: float) -> float:
    """
    Calculate atomic time in seconds from unix time in seconds.

    :param unix_s: Unix time in seconds

    :return: atomic time (tai) in seconds
    """
    unix_time = Time(unix_s, format="unix")
    ska_epoch_tai = Time(SKA_EPOCH, scale="utc").unix_tai
    return unix_time.unix_tai - ska_epoch_tai


def get_current_tai_timestamp() -> float:
    """Get the current time as a TAI timestamp."""
    return get_tai_timestamp_from_unix_s(time.time())
