# flake8: noqa
# pylint: disable=no-else-return, too-many-locals
# pylint: disable=unnecessary-comprehension, unspecified-encoding
# pylint: disable=R0201,C0116,W1514,R0903,R0913
"""Contains pytest fixtures for other tests setup."""
import datetime
import logging
import os
import re
from dataclasses import dataclass, field
from typing import List, Tuple

import pyhdbpp
import pytest
import tango
from dish_enums import (
    Band,
    DishMode,
    DSOperatingMode,
    IndexerPosition,
    SPFCapabilityStates,
    SPFOperatingMode,
)
from log_parser import DishLMCEventsAndLogsFileParser
from utils import EventStore, tango_dev_proxy

LOGGER = logging.getLogger(__name__)


def pytest_sessionstart(session):
    """
    Pytest hook; prints info about tango version.
    :param session: a pytest Session object
    :type session: :py:class:`pytest.Session`
    """
    print(tango.utils.info())


# pylint: disable=missing-function-docstring
def pytest_addoption(parser):
    parser.addoption(
        "--dish-manager-fqdn",
        action="store",
        default="mid-dish/dish-manager/SKA001",
        help="FQDN for the DishManager device",
    )
    parser.addoption(
        "--dish-structure-fqdn",
        action="store",
        default="mid-dish/ds-manager/SKA001",
        help="FQDN for the DS device",
    )
    parser.addoption(
        "--spf-fqdn",
        action="store",
        default="mid-dish/simulator-spfc/SKA001",
        help="FQDN for the SPF device",
    )
    parser.addoption(
        "--spfrx-fqdn",
        action="store",
        default="mid-dish/simulator-spfrx/SKA001",
        help="FQDN for the SPFRx device",
    )
    parser.addoption(
        "--ds-simulator-namespace",
        action="store",
        default="http://skao.int/DS_ICD/",
        help="Namespace for the DS simulator",
    )
    parser.addoption(
        "--ds-simulator-url",
        action="store",
        default="opc.tcp://ds-opcua-server-simulator-001-svc:4840/dish-structure/server/",
        help="URL for the DS simulator",
    )
    parser.addoption(
        "--dish-logger-fqdn",
        action="store",
        default="mid-dish/dish-logger/SKA001",
        help="FQDN for the Dish logger device",
    )
    # pyhdbpp reader configuration string
    parser.addoption(
        "--config",
        action="store",
        help="Details of TimescaleDB credentials for the reader fixture",
    )
    parser.addoption(
        "--event-storage-files-path",
        action="store",
        default=None,
        help="File path to store event tracking files to",
    )
    parser.addoption(
        "--event-and-log-storage-files-path",
        action="store",
        default=None,
        help="File path to store combined event and log tracking files to",
    )
    parser.addoption(
        "--sequence-diagram-storage-files-path",
        action="store",
        default=None,
        help="File path to store sequence diagrams generated from combined event and logs",
    )
    parser.addoption(
        "--no-reset-between-tests",
        action="store_true",
        default=False,
        help="Flag to disable reseting devices between tests",
    )


@pytest.fixture(scope="session")
def dish_manager_fqdn(request):
    return request.config.getoption("--dish-manager-fqdn")


@pytest.fixture(scope="session")
def dish_structure_fqdn(request):
    return request.config.getoption("--dish-structure-fqdn")


@pytest.fixture(scope="session")
def spf_fqdn(request):
    return request.config.getoption("--spf-fqdn")


@pytest.fixture(scope="session")
def spfrx_fqdn(request):
    return request.config.getoption("--spfrx-fqdn")


@pytest.fixture(scope="session")
def dish_logger_fqdn(request):
    return request.config.getoption("--dish-logger-fqdn")


@pytest.fixture(name="dish_manager", scope="module")
def dish_manager_device_proxy(dish_manager_fqdn):
    return tango_dev_proxy(dish_manager_fqdn, LOGGER)


@pytest.fixture(name="dish_structure", scope="module")
def dish_structure_device_proxy(dish_structure_fqdn):
    return tango_dev_proxy(dish_structure_fqdn, LOGGER)


@pytest.fixture(name="spf", scope="module")
def spf_device_proxy(spf_fqdn):
    return tango_dev_proxy(spf_fqdn, LOGGER)


@pytest.fixture(name="spfrx", scope="module")
def spfrx_device_proxy(spfrx_fqdn):
    return tango_dev_proxy(spfrx_fqdn, LOGGER)


@pytest.fixture(name="dish_logger", scope="module")
def dish_logger_device_proxy(dish_logger_fqdn):
    return tango_dev_proxy(dish_logger_fqdn, LOGGER)


@pytest.fixture(scope="session")
def no_reset_between_tests(request):
    """Returns the option whether to reset between tests or not."""
    return request.config.getoption("--no-reset-between-tests")


@pytest.fixture(scope="module")
def ds_simulator_namespace(request):
    """Returns the DS simulator OPC-UA namespace."""
    ds_sim_ns = request.config.getoption("--ds-simulator-namespace")
    return ds_sim_ns


@pytest.fixture(scope="module")
def ds_simulator_url(request):
    """Returns the DS simulator OPC-UA URL."""
    ds_sim_url = request.config.getoption("--ds-simulator-url")
    return ds_sim_url


@pytest.fixture(scope="module")
def reader(request):
    """Returns the pybdbpp timescaleDB reader."""
    apiclass = "pyhdbpp.timescaledb.timescaledb.TimescaleDbReader"
    config = request.config.getoption("--config")

    # This is the reader with the recieved configuration
    return pyhdbpp.reader(apiclass=apiclass, config=config)


@pytest.fixture
def dev_name(request, dish_manager_fqdn, dish_structure_fqdn, spf_fqdn, spfrx_fqdn):
    if request.param == "dish_manager_fqdn":
        return dish_manager_fqdn
    elif request.param == "dish_structure_fqdn":
        return dish_structure_fqdn
    elif request.param == "spf_fqdn":
        return spf_fqdn
    else:
        return spfrx_fqdn


@pytest.fixture
def modes_command_map():
    return {
        "STANDBY_LP": "SetStandbyLPMode",
        "STANDBY_FP": "SetStandbyFPMode",
        "OPERATE": "SetOperateMode",
        "STOW": "SetStowMode",
    }


@pytest.fixture
def event_store():
    """Fixture for storing events"""
    return EventStore()


@pytest.fixture(scope="module")
def event_store_class():
    """Event store class"""
    return EventStore


@pytest.fixture
def init_devices(event_store, dish_manager, dish_structure, spf, spfrx, no_reset_between_tests):
    """Reset the tango devices to a fresh state before each test."""
    if no_reset_between_tests:
        return

    # abort commands if stuck
    lrc_status = dish_manager.longRunningCommandStatus
    lrc_status = ", ".join([evt for evt in lrc_status])
    if "IN_PROGRESS" in lrc_status:
        # this wait is very important for our AUTOMATED tests!!!
        # wait for task status updates to finish before resetting the
        # sub devices to a clean state for the next test. Reasons are:
        # [*] your command map may never evaluate true for the
        # awaited value to report the final task status of the LRC.
        # [*] the base classes needs this final task status to allow the
        # subsequently issued commands to be moved from queued to in progress

        # another approach will be to ensure that all tests check the
        # command status for every issued command as part of its assert
        event_store.get_queue_events(timeout=5)

    LOGGER.debug("Reset spf and spfrx simulators")
    spfrx.SetStandbyMode()
    spf.SetStandbyLPMode()

    dish_structure.subscribe_event(
        "operatingMode",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )
    dish_structure.subscribe_event(
        "indexerPosition",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )

    LOGGER.debug("Reset dish structure manager")
    if dish_structure.operatingMode != DSOperatingMode.STOW:
        dish_structure.Stow()
        event_store.wait_for_value(DSOperatingMode.STOW, timeout=60)

    if dish_structure.indexerPosition != IndexerPosition.B1:
        dish_structure.SetIndexPosition(IndexerPosition.B1)
        event_store.wait_for_value(IndexerPosition.B1, timeout=60)

    dish_structure.SetStandbyLPMode()
    event_store.wait_for_value(DSOperatingMode.STANDBY_LP, timeout=9)

    spf.subscribe_event(
        "operatingMode",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )
    assert event_store.wait_for_value(SPFOperatingMode.STANDBY_LP, timeout=7)
    event_store.clear_queue()

    LOGGER.debug("Sync component state on dish manager")
    dish_manager.SyncComponentStates()

    dish_manager.subscribe_event(
        "dishMode",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )

    try:
        event_store.wait_for_value(DishMode.STANDBY_LP, timeout=7)
    except RuntimeError as err:
        component_states = dish_manager.GetComponentStates()
        raise RuntimeError(f"DishManager not in STANDBY_LP:\n {component_states}\n") from err

    # log the component_states for debugging
    LOGGER.debug(f"DM component state: {dish_manager.GetComponentStates()}")

    yield


@pytest.fixture
def dish_freq_band_configuration(
    dish_manager,
):
    """
    A helper that manages dish lmc frequency band configuration
    """

    class _BandSelector:
        def go_to_band(self, band_number):
            if dish_manager.configuredBand.name == f"B{band_number}":
                LOGGER.info("Dish master is already at requested band")
                return

            allowed_modes = ["STANDBY_FP", "STOW", "OPERATE"]
            current_dish_mode = dish_manager.dishMode.name
            if not (current_dish_mode in allowed_modes):
                LOGGER.info(
                    f"Dish master cannot request ConfigureBand while in {current_dish_mode}"
                )
                return

            event_cb = EventStore()
            dish_manager.subscribe_event(
                "configuredBand",
                tango.EventType.CHANGE_EVENT,
                event_cb,
            )

            dishmode_event_store = EventStore()
            dish_manager.subscribe_event(
                "dishMode",
                tango.EventType.CHANGE_EVENT,
                dishmode_event_store,
            )

            [[_], [unique_id]] = dish_manager.command_inout(f"ConfigureBand{band_number}", False)

            event_cb.wait_for_value(Band(int(band_number)), timeout=60)
            dishmode_event_store.wait_for_value(DishMode.CONFIG, timeout=8)
            dishmode_event_store.wait_for_value(getattr(DishMode, current_dish_mode), timeout=8)

            # For some reason the attribute updates lag the events sometimes
            # Or the command may just be stuck in the progress
            # Redoing the subscription and waiting for more updates
            if dish_manager.configuredBand.name != f"B{band_number}":
                # abort commands if stuck
                lrc_status = dish_manager.longRunningCommandStatus
                lrc_status = ", ".join([evt for evt in lrc_status])
                if f"{unique_id}, IN_PROGRESS" in lrc_status:
                    dish_manager.AbortCommands()

                LOGGER.info("Redoing the sub to catch lagging updates")
                event_cb.clear_queue()
                [[_], [_]] = dish_manager.command_inout(f"ConfigureBand{band_number}", False)
                dish_manager.subscribe_event(
                    "configuredBand",
                    tango.EventType.CHANGE_EVENT,
                    event_cb,
                )
                event_cb.wait_for_value(Band(int(band_number)), timeout=60)

            assert dish_manager.configuredBand.name == f"B{band_number}"
            LOGGER.info(f"{dish_manager} successfully transitioned to Band {band_number}")

    return _BandSelector()


@pytest.fixture
def spfc_sim_mode_transition(spf):
    class _SPFCFastTransition:
        def spfc_lp_workaround(self):
            spf.SetFeedMode([2, 0])

        def spfc_operate_workaround(self):
            spf.SetFeedMode([2, 0])
            spf.SetFeedMode([2, 1])

        def spfc_fp_workaround(self):
            event_cb = EventStore()
            spf.subscribe_event(
                "b2CapabilityState",
                tango.EventType.CHANGE_EVENT,
                event_cb,
            )

            # Wait between the two attributes set
            spf.SetFeedMode([2, 0])
            spf.SetFeedMode([2, 1])
            spf.b2LnaPidTempSetPoint = 4
            event_cb.get_queue_values(timeout=10)
            spf.b2LnaPidPowerState = True
            # Wait 2 minutes for b2CapabilityState to transition to OPERATE-FULL
            event_cb.wait_for_value(SPFCapabilityStates["OPERATE_FULL"], timeout=120)

    return _SPFCFastTransition()


@pytest.fixture
def modes_helper(
    dish_manager,
    modes_command_map,
    dish_freq_band_configuration,
):
    """
    A helper that manages device modes using events
    """

    class _ModesHelper:
        def ensure_dish_manager_mode(self, desired_mode_name):
            """Move dish manager to desired mode.
            Via STANDBY_FP, to ensure any mode can move to any mode.
            """
            if dish_manager.dishMode.name == desired_mode_name:
                LOGGER.info("Dish manager is already at requested mode")
                return

            # handle case where dish mode is unknown
            if dish_manager.dishMode.name == "UNKNOWN":
                self.dish_manager_go_to_mode("STOW")
                if desired_mode_name == "STOW":
                    return

            if desired_mode_name == "STOW":
                self.dish_manager_go_to_mode("STOW")
                return
            elif desired_mode_name == "STANDBY_FP":
                self.dish_manager_go_to_mode("STANDBY_FP")
                return
            else:
                # transition to desired mode through STANDBY_FP
                self.dish_manager_go_to_mode("STANDBY_FP")
                self.dish_manager_go_to_mode(desired_mode_name)

        def dish_manager_go_to_mode(self, desired_mode_name):
            """Move device to desired dish mode"""
            if dish_manager.dishMode.name == desired_mode_name:
                LOGGER.info("Dish manager is already at requested mode")
                return

            # make sure there is a configured
            # band if the requested mode is operate
            if desired_mode_name == "OPERATE" and dish_manager.configuredBand.name in [
                "NONE",
                "UNKNOWN",
            ]:
                dish_freq_band_configuration.go_to_band(2)

            event_cb = EventStore()
            dish_manager.subscribe_event(
                "dishMode",
                tango.EventType.CHANGE_EVENT,
                event_cb,
            )

            # Move to desired mode
            command_name = modes_command_map[desired_mode_name]
            LOGGER.info(
                f"Moving {dish_manager} from "
                f"{dish_manager.dishMode.name} to {desired_mode_name}"
            )

            LOGGER.info(f"{dish_manager} executing {command_name} ")
            [[_], [unique_id]] = dish_manager.command_inout(command_name)

            # wait for events
            event_cb.wait_for_value(DishMode[desired_mode_name], timeout=60)

            # For some reason the attribute updates lag the events sometimes
            # Or the command may just be stuck in the progress
            # Redoing the subscription and waiting for more updates
            if dish_manager.dishMode.name != desired_mode_name:
                # abort commands if stuck
                lrc_status = dish_manager.longRunningCommandStatus
                lrc_status = ", ".join([evt for evt in lrc_status])
                if f"{unique_id}, IN_PROGRESS" in lrc_status:
                    dish_manager.AbortCommands()

                LOGGER.info("Redoing the sub to catch lagging updates")
                event_cb.clear_queue()
                [[_], [_]] = dish_manager.command_inout(command_name)
                dish_manager.subscribe_event(
                    "dishMode",
                    tango.EventType.CHANGE_EVENT,
                    event_cb,
                )
                event_cb.wait_for_value(DishMode[desired_mode_name], timeout=60)

            assert dish_manager.dishMode.name == desired_mode_name
            LOGGER.info(f"{dish_manager} successfully transitioned to {desired_mode_name} mode")

    return _ModesHelper()


@dataclass
class TrackedDevice:
    """Class to group tracked device information"""

    device_proxy: tango.DeviceProxy
    attribute_names: Tuple[str]
    subscription_ids: List[int] = field(default_factory=list)


class EventPrinter:
    """Class that writes attribute changes to a file"""

    def __init__(self, filename: str, tracked_devices: Tuple[TrackedDevice] = ()) -> None:
        self.tracked_devices = tracked_devices
        self.filename = filename
        self.events = []

    def __enter__(self):
        for tracked_device in self.tracked_devices:
            dp = tracked_device.device_proxy
            for attr_name in tracked_device.attribute_names:
                sub_id = dp.subscribe_event(attr_name, tango.EventType.CHANGE_EVENT, self)
                tracked_device.subscription_ids.append(sub_id)

    def __exit__(self, exc_type, exc_value, exc_tb):
        for tracked_device in self.tracked_devices:
            try:
                dp = tracked_device.device_proxy
                for sub_id in tracked_device.subscription_ids:
                    dp.unsubscribe_event(sub_id)
            except tango.DevError:
                pass

    def add_event(self, timestamp, message):
        self.events.append((timestamp, message))
        with open(self.filename, "a") as open_file:
            open_file.write("\n" + message)

    def push_event(self, ev: tango.EventData):
        event_string = ""
        if ev.err:
            err = ev.errors[0]
            event_string = f"\nEvent Error {err.desc} {err.origin} {err.reason}"
        else:
            attr_name = ev.attr_name.split("/")[-1]
            attr_value = ev.attr_value.value
            if ev.attr_value.type == tango.CmdArgType.DevEnum:
                attr_value = ev.device.get_attribute_config(attr_name).enum_labels[attr_value]

            event_string = f"Event - {ev.reception_date}\t{ev.device}\t{attr_name}\t{attr_value}"

        self.add_event(ev.reception_date.totime(), event_string)


def get_iso_date_string_from_string(val):
    iso_date_string_pattern = r"\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}.\d{3}Z"
    match = re.search(iso_date_string_pattern, val)
    return match.group() if match else ""


def get_pod_logs_and_timestamps(namespace, pod_name, since_time):
    command = f"kubectl logs {pod_name} -n {namespace} --since-time={since_time}"

    with os.popen(command) as stream:
        api_response = stream.read()
    logs = api_response.splitlines()

    # Extract times from each line in the logs (e.g. 2024-08-26T07:16:11.051Z)
    extracted_logs = []
    for log in logs:
        # Search for the timestamp in each log entry
        iso_date_string = get_iso_date_string_from_string(log)
        if iso_date_string != "":
            datetime_obj = datetime.datetime.strptime(iso_date_string, "%Y-%m-%dT%H:%M:%S.%fZ")
            extracted_logs.append((datetime_obj.timestamp(), f" Log  - {log}"))
        else:
            print(f"No timestamp found in log: {log}")

    return extracted_logs


@pytest.fixture(scope="function")
def monitor_tango_servers(
    request: pytest.FixtureRequest, dish_manager, dish_structure, spf, spfrx
):
    event_files_dir = request.config.getoption("--event-storage-files-path")
    event_and_log_files_dir = request.config.getoption("--event-and-log-storage-files-path")

    if event_files_dir is None or event_and_log_files_dir is None:
        yield None
        return

    if not os.path.exists(event_files_dir):
        os.makedirs(event_files_dir)
    if not os.path.exists(event_and_log_files_dir):
        os.makedirs(event_and_log_files_dir)

    events_file_name = ".".join((f"events_{request.node.name}", "txt"))
    events_file_path = os.path.join(event_files_dir, events_file_name)

    events_and_logs_file_name = ".".join((f"events_and_logs_{request.node.name}", "txt"))
    events_and_logs_file_path = os.path.join(event_and_log_files_dir, events_and_logs_file_name)

    dm_tracker = TrackedDevice(
        dish_manager,
        (
            "dishmode",
            # "capturing", TODO push event is disabled
            "healthstate",
            "pointingstate",
            "b1capabilitystate",
            "b2capabilitystate",
            "b3capabilitystate",
            "b4capabilitystate",
            "b5acapabilitystate",
            "b5bcapabilitystate",
            "achievedtargetlock",
            "achievedpointing",
            "configuredband",
            "spfconnectionstate",
            "spfrxconnectionstate",
            "dsconnectionstate",
            "longrunningcommandstatus",
            "longrunningcommandresult",
            "longrunningcommandprogress",
        ),
    )
    ds_tracker = TrackedDevice(
        dish_structure,
        (
            "operatingMode",
            "powerState",
            "healthState",
            "pointingState",
            "indexerPosition",
            "achievedPointing",
            "longrunningcommandstatus",
            "longrunningcommandresult",
            "longrunningcommandprogress",
        ),
    )
    spf_tracker = TrackedDevice(
        spf,
        (
            "operatingMode",
            "powerState",
            "healthState",
            "bandInFocus",
            "b1capabilitystate",
            "b2capabilitystate",
            "b3capabilitystate",
            "b4capabilitystate",
            "b5acapabilitystate",
            "b5bcapabilitystate",
        ),
    )
    spfrx_tracker = TrackedDevice(
        spfrx,
        (
            "operatingMode",
            "capturingData",
            "configuredBand",
            "healthState",
            "b1capabilitystate",
            "b2capabilitystate",
            "b3capabilitystate",
            "b4capabilitystate",
            "b5acapabilitystate",
            "b5bcapabilitystate",
        ),
    )
    event_printer = EventPrinter(
        events_file_path, (dm_tracker, ds_tracker, spf_tracker, spfrx_tracker)
    )

    with event_printer:
        test_start_time = datetime.datetime.now(datetime.timezone.utc)
        iso_start_time = test_start_time.isoformat()
        event_printer.add_event(test_start_time.timestamp(), "Events set up, test starting")

        yield

        # Save logs from the pods
        all_pod_logs = {}
        namespace = os.getenv("KUBE_NAMESPACE")

        if namespace is not None:
            for device in ["ds-dsmanager-001-0", "ds-dishmanager-001-0"]:
                all_pod_logs[device] = get_pod_logs_and_timestamps(
                    namespace, device, iso_start_time
                )

        # combine and sort logs/events
        captured_events = event_printer.events
        combined_events_and_logs = []

        for logs in all_pod_logs.values():
            combined_events_and_logs.extend(logs)

        combined_events_and_logs.extend(captured_events)

        combined_events_and_logs.sort(key=lambda x: x[0])

        # Write the combined entries to the output file
        with open(events_and_logs_file_path, "w") as file:
            for timestamp, message in combined_events_and_logs:
                file.write(f"{timestamp:.3f} - {message}\n")

        # Generate sequence diagram from the combined events and logs file
        sequence_diagram_files_dir = request.config.getoption(
            "--sequence-diagram-storage-files-path"
        )

        if sequence_diagram_files_dir is not None:
            if not os.path.exists(sequence_diagram_files_dir):
                os.makedirs(sequence_diagram_files_dir)

            sequence_diagram_file_name = f"sequence_diagram_{request.node.name}.puml"

            sequence_diagram_file_path = os.path.join(
                sequence_diagram_files_dir, sequence_diagram_file_name
            )

            file_parser = DishLMCEventsAndLogsFileParser(
                show_events=False,
                show_component_state_updates=False,
            )

            # Generate a sequence diagram
            file_parser.parse(
                events_and_logs_file_path, sequence_diagram_file_path, actor="pytest"
            )

            # Generate a more verbose diagram with events and component state updates
            verbose_diagrams_dir = os.path.join(sequence_diagram_files_dir, "verbose")

            if not os.path.exists(verbose_diagrams_dir):
                os.makedirs(verbose_diagrams_dir)

            verbose_sequence_diagram_file_path = os.path.join(
                verbose_diagrams_dir, sequence_diagram_file_name
            )

            file_parser.show_events = True
            file_parser.show_component_state_updates = True

            file_parser.parse(
                events_and_logs_file_path, verbose_sequence_diagram_file_path, actor="pytest"
            )
