# pylint: disable=C0116,R0914
"""
Verify that dish lmc receives timestamped Az/El pointing coordinates

jira requirement: L2-4708, L2-4692, L2-4704, L2-4639
"""


import logging

import pytest
import tango
from dish_enums import PointingState
from pytest import approx
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from utils import get_current_tai_timestamp, retrieve_attr_value

LOGGER = logging.getLogger(__name__)

# constants are from ska-sim-dishmaster
MAX_DESIRED_AZIM = 270.0
MIN_DESIRED_AZIM = -270.0
MAX_DESIRED_ELEV = 90.0
MIN_DESIRED_ELEV = 15.0

# unit for drive rate in degrees per second
AZIM_DRIVE_MAX_RATE = 3.0
ELEV_DRIVE_MAX_RATE = 1.0

# TODO (ST:04/2024) find out how configureTargetLock attribute works out the threshold
TOLERANCE = 1e-2  # MeerKAT lock threshold


@pytest.mark.xfail(reason="Awaiting dish manager release which writes achievedTargetLock")
@pytest.mark.acceptance
@scenario("XTP-5414.feature", "Test dish Track command")
def test_dish_pointing(monitor_tango_servers, init_devices):
    pass


@pytest.mark.xfail(reason="Prequisite track command test xfails")
@pytest.mark.acceptance
@scenario("XTP-5414.feature", "Test Dish TrackStop command")
def test_dish_track_stop_command(monitor_tango_servers):
    pass


@given(parse("dish_manager dishMode reports {dish_mode}"))
def check_dish_manager_dish_mode(dish_mode, dish_manager, modes_helper):
    modes_helper.ensure_dish_manager_mode(dish_mode)
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    LOGGER.info(f"{dish_manager} dishMode: {current_dish_mode}")


@given(parse("dish_manager pointingState reports {pointing_state}"))
def check_dish_reports_expected_pointing_state(pointing_state, dish_manager, event_store_class):
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "pointingState",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    event_cb.wait_for_value(PointingState[pointing_state])

    current_pointing_state = retrieve_attr_value(dish_manager, "pointingState")
    assert current_pointing_state == pointing_state
    LOGGER.info(f"{dish_manager} pointingState: {current_pointing_state}")


@given(parse("dish_manager pointingState reports {track} or {slew}"))
def dish_reports_required_pointing_state(track, slew, dish_manager, event_store_class):
    """Test Dish Manager reports the correct pointing state"""
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "pointingState",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )

    event_cb.wait_for_value(PointingState.TRACK)
    current_pointing_state = retrieve_attr_value(dish_manager, "pointingState")
    assert current_pointing_state in [track, slew]


@given(parse("dish_structure pointingState reports {track} or {slew}"))
def dish_structure_reports_required_pointing_state(track, slew, dish_structure):
    current_pointing_state = retrieve_attr_value(dish_structure, "pointingState")
    assert current_pointing_state in [track, slew]
    LOGGER.info(f"{dish_structure} pointingState: {current_pointing_state}")


@given("dish_manager receives pointing updates on the programTrackTable")
def check_dish_receives_pointing_coordinates(dish_manager):
    current_pointing = dish_manager.achievedPointing
    current_az = current_pointing[1]
    current_el = current_pointing[2]

    current_time_tai_s = get_current_tai_timestamp()

    # Directions to move values
    az_dir = 1 if current_az < 260 else -1
    el_dir = 1 if current_el < 80 else -1

    initial_el = current_el if current_el < 80 else 80

    sample_table = [
        current_time_tai_s + 3,
        current_az + 0.25 * az_dir,
        initial_el + 0.25 * el_dir,
        current_time_tai_s + 5,
        current_az + 0.5 * az_dir,
        initial_el + 0.5 * el_dir,
        current_time_tai_s + 7,
        current_az + 0.75 * az_dir,
        initial_el + 0.75 * el_dir,
        current_time_tai_s + 9,
        current_az + 1 * az_dir,
        initial_el + 1 * el_dir,
        current_time_tai_s + 11,
        current_az + 1.25 * az_dir,
        initial_el + 1.25 * el_dir,
    ]

    dish_manager.programTrackTable = sample_table


@when("I issue Track on dish_manager", target_fixture="pointing_data_store")
def track_requested_pointing(event_store_class, dish_manager):
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "pointingState",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    # transitions to SLEW pointingState
    dish_manager.Track()

    return event_cb


@when("I issue TrackStop on dish_manager")
def issue_track_stop_on_dish(dish_manager):
    dish_manager.TrackStop()


@then("dish_manager should slew to the commanded position")
def check_dish_manager_slews_to_the_position(pointing_data_store):
    pointing_state_events = pointing_data_store

    # check that the pointing state transitions to SLEW
    pointing_state_events.wait_for_value(PointingState["SLEW"])


@then(parse("dish_manager pointingState should transition to {track} on target"))
def check_pointing_state_on_target(track, dish_manager, event_store_class):
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "pointingState",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )

    event_cb.wait_for_value(PointingState[track], timeout=30)


@then("dish_manager achievedTargetLock should transition to True")
def check_achieved_target_lock(dish_manager, event_store_class):
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        "achievedTargetLock",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )

    event_cb.wait_for_value(True)


@then(
    "the difference between actual and desired position "
    "should be less than or equal to the configured threshold"
)
def check_dish_manager_locked_on_target(dish_manager):
    achieved_az, achieved_el = (
        dish_manager.achievedPointing[1],
        dish_manager.achievedPointing[2],
    )

    desired_az, desired_el = (
        dish_manager.desiredPointing[1],
        dish_manager.desiredPointing[2],
    )

    assert desired_az == approx(achieved_az, rel=TOLERANCE)
    assert desired_el == approx(achieved_el, rel=TOLERANCE)


@then(parse("dish_manager pointingState should transition to {pointing_state}"))
def dish_manager_transitions_to_expected_pointing_state(
    pointing_state, dish_manager, event_store_class
):
    dm_pointing_state_events = event_store_class()
    dish_manager.subscribe_event(
        "pointingState",
        tango.EventType.CHANGE_EVENT,
        dm_pointing_state_events,
    )

    dm_pointing_state_events.wait_for_value(PointingState[pointing_state], timeout=60)
    current_pointing_state = retrieve_attr_value(dish_manager, "pointingState")
    assert current_pointing_state == pointing_state


@then(parse("dish_structure pointingState should transition to {pointing_state}"))
def dish_structure_transitions_to_expected_pointing_state(pointing_state, dish_structure):
    current_pointing_state = retrieve_attr_value(dish_structure, "pointingState")
    assert current_pointing_state == pointing_state
