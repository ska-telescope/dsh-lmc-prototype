# pylint: disable=missing-function-docstring
"""
Verify that dish executes STOW transition within 1 second

jira requirement: L2-4598
"""

import time

import pytest
import tango
from pytest_bdd import given, scenario, then
from pytest_bdd.parsers import parse


@pytest.mark.acceptance
@scenario("XTP-39717.feature", "Dish LMC executes STOW to DS within 1s of issue from TMC")
def test_dish_executes_stow_within_one_second(monitor_tango_servers):
    pass


@given("dish_manager receives a STOW request from a client", target_fixture="event_objects")
def dish_is_requested_to_stow(dish_structure, modes_helper, event_store_class):
    # ensure that dish is not reporting stow already before issuing the request
    modes_helper.ensure_dish_manager_mode("STANDBY_FP")

    ds_event_cb = event_store_class()
    dish_structure.subscribe_event(
        "longRunningCommandResult",
        tango.EventType.CHANGE_EVENT,
        ds_event_cb,
    )

    stow_trigger_time = time.time()
    modes_helper.ensure_dish_manager_mode("STOW")

    return stow_trigger_time, ds_event_cb


@then(
    parse("dish_structure should execute STOW within {duration:g} second of receiving the command")
)
def check_dish_manager_fans_out_stow_within_a_second(duration, event_objects):
    stow_trigger_time, ds_event_cb = event_objects

    ds_expected_result_update = '"Stow Called"'
    time_ds_result_received = None
    ds_event_cb = ds_event_cb.get_queue_events()

    for event_data in ds_event_cb:
        if event_data.attr_value.value[1] == ds_expected_result_update:
            time_ds_result_received = event_data.reception_date.totime()

    # Check that stow command was executed within 1 second on Dish Structure
    assert time_ds_result_received - stow_trigger_time <= duration
