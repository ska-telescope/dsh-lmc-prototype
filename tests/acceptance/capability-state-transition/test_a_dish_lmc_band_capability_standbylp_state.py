# pylint: disable=W0107,C0116
"""
Verify that LMC reports Band_X Capability as STANDBY state if DSH is in STANDBY-LP mode

jira requirement: L2-4697, L2-4695, L2-4651
"""
import logging

import pytest
import tango
from dish_enums import CapabilityStates, SPFCapabilityStates, SPFRxCapabilityStates
from pytest_bdd import given, scenario, then
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario(
    "XTP-6269.feature",
    "Dish LMC Reports DSH Capability Standby in LP Mode",
)
def test_dish_manager_capability_state_reports_standby_in_lp_mode(monitor_tango_servers):
    """Test that dish lmc reports STANDBY capability state"""
    pass


@given(parse("dish_manager dishMode reports {dish_mode}"))
def check_dish_manager_dish_mode(dish_mode, dish_manager, modes_helper):
    # convert dish mode to have underscore
    # for DishMode STANDBY_LP enum in utils
    dish_mode = dish_mode.replace("-", "_")

    modes_helper.ensure_dish_manager_mode(dish_mode)
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    LOGGER.info(f"{dish_manager} dishMode: {current_dish_mode}")


@given(parse("spfrx b{band_number}CapabilityState reports {expected_state}"))
def check_spfrx_capability_state(band_number, expected_state, spfrx, event_store):
    spfrx.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )
    event_store.wait_for_value(SPFRxCapabilityStates[expected_state], timeout=10)

    b_x_capability_state = retrieve_attr_value(spfrx, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{spfrx} b{band_number}CapabilityState: {b_x_capability_state}")


@given(parse("spf b{band_number}CapabilityState reports {expected_state}"))
def check_spf_capability_state(
    band_number,
    expected_state,
    spf,
    event_store,
    spfc_sim_mode_transition,
):
    spf.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )
    if "b2LnaPidPowerState" in spf.get_attribute_list():
        spfc_sim_mode_transition.spfc_lp_workaround()
    else:
        event_store.wait_for_value(SPFCapabilityStates[expected_state], timeout=10)
        band_number = 5 if band_number.startswith("5") else band_number
    b_x_capability_state = retrieve_attr_value(spf, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{spf} b{band_number}CapabilityState: {b_x_capability_state}")


@then(parse("dish_manager b{band_number}CapabilityState should report {expected_state}"))
def check_dish_capability_state(band_number, expected_state, event_store, dish_manager):
    dish_manager.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        event_store,
    )
    b_x_capability_state = retrieve_attr_value(dish_manager, f"b{band_number}CapabilityState")
    event_store.wait_for_value(CapabilityStates[expected_state], timeout=10)
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{dish_manager} b{band_number}CapabilityState: {b_x_capability_state}")
