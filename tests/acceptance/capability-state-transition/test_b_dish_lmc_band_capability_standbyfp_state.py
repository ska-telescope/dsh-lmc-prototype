# pylint: disable=W0107,R0913,C0116
"""
Verify that LMC reports Band_X Capability as STANDBY state if DSH is in STANDBY-FP mode

jira requirement: L2-4697, L2-4695, L2-4651
"""
import logging

import pytest
import tango
from dish_enums import CapabilityStates, SPFCapabilityStates, SPFRxCapabilityStates
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.fixture(scope="module")
def spf_cap_state_events(event_store_class):
    """Fixture for storing dish_manager events"""
    return event_store_class()


@pytest.fixture(scope="module")
def rx_cap_state_events(event_store_class):
    """Fixture for storing dish_manager events"""
    return event_store_class()


@pytest.fixture(scope="module")
def dm_cap_state_events(event_store_class):
    """Fixture for storing dish_manager events"""
    return event_store_class()


@pytest.mark.acceptance
@scenario(
    "XTP-15471.feature",
    "Dish LMC Reports DSH Capability Standby in FP mode",
)
def test_dish_manager_capability_state_reports_standby_in_fp_mode(monitor_tango_servers):
    """Test that dish lmc reports STANDBY capability state"""
    pass


@given(parse("dish_manager dishMode reports {dish_mode}"))
def check_dish_manager_dish_mode(
    dish_mode,
    dish_manager,
    modes_helper,
):
    # convert dish mode to have underscore
    # for DishMode STANDBY_FP enum in utils
    dish_mode = dish_mode.replace("-", "_")

    modes_helper.ensure_dish_manager_mode(dish_mode)
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    LOGGER.info(f"{dish_manager} dishMode: {current_dish_mode}")


@when(parse("I issue ConfigureBand{band_number} on dish_manager"))
def configure_band(
    band_number,
    dish_freq_band_configuration,
    spf,
    spfrx,
    dish_manager,
    dm_cap_state_events,
    rx_cap_state_events,
    spf_cap_state_events,
):
    spf.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        spf_cap_state_events,
    )

    spfrx.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        rx_cap_state_events,
    )

    dish_manager.subscribe_event(
        f"b{band_number}CapabilityState",
        tango.EventType.CHANGE_EVENT,
        dm_cap_state_events,
    )

    dish_freq_band_configuration.go_to_band(band_number)


@then(parse("spfrx b{band_number}CapabilityState should report {expected_state}"))
def check_spfrx_capability_state(band_number, expected_state, spfrx, rx_cap_state_events):
    rx_cap_state_events.wait_for_value(SPFRxCapabilityStates[expected_state], timeout=10)
    b_x_capability_state = retrieve_attr_value(spfrx, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state
    LOGGER.info(f"{spfrx} b{band_number}CapabilityState: {b_x_capability_state}")


@then(parse("spf b{band_number}CapabilityState should report {expected_state}"))
def check_spf_capability_state(
    band_number,
    expected_state,
    spf,
    spf_cap_state_events,
    spfc_sim_mode_transition,
):
    # convert expected state to have underscore
    # for SPFCapabilityStates OPERATE_FULL enum
    expected_state = expected_state.replace("-", "_")

    if "b2LnaPidPowerState" in spf.get_attribute_list():
        spfc_sim_mode_transition.spfc_fp_workaround()
    else:
        spf_cap_state_events.wait_for_value(SPFCapabilityStates[expected_state], timeout=10)
        band_number = 5 if band_number.startswith("5") else band_number
    b_x_capability_state = retrieve_attr_value(spf, f"b{band_number}CapabilityState")
    assert b_x_capability_state.replace("-", "_") == expected_state
    LOGGER.info(f"{spf} b{band_number}CapabilityState: {b_x_capability_state}")


@then(parse("dish_manager b{band_number}CapabilityState should report {expected_state}"))
def check_dish_capability_state(band_number, expected_state, dm_cap_state_events, dish_manager):
    dm_cap_state_events.wait_for_value(CapabilityStates[expected_state], timeout=20)

    b_x_capability_state = retrieve_attr_value(dish_manager, f"b{band_number}CapabilityState")
    assert b_x_capability_state == expected_state

    LOGGER.info(f"{dish_manager} b{band_number}CapabilityState: {b_x_capability_state}")
