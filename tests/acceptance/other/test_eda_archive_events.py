"""
Verify an attribute reading is in the EDA
"""

import datetime

import pytest
import tango


@pytest.mark.verify_eda
def test_changing_k_value(dish_manager, event_store_class, reader):
    """Test changing the k value and read it from EDA"""
    event_store = event_store_class()
    dish_manager.subscribe_event(
        "kValue",
        tango.EventType.ARCHIVE_EVENT,
        event_store,
    )

    new_k_value = dish_manager.kvalue + 1
    dish_manager.SetKvalue(new_k_value)
    start_time = datetime.datetime.utcnow()
    event_store.wait_for_value(new_k_value)
    end_time = start_time + datetime.timedelta(minutes=1)

    host = "tango-databaseds.miditf-lmc-003-karoo-sims.svc.miditf.internal.skao.int"
    data = reader.get_attribute_values(
        f"tango://{host}:10000/mid-dish/dish-manager/SKA003/kvalue",
        start_time,
        end_time,
    )
    assert data[0][1] == new_k_value
