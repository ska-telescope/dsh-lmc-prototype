# pylint: disable=C0114,W0511
import logging

import pytest

LOGGER = logging.getLogger(__name__)


# TODO Determine how to use this fixture for SPF integration in the ITF
@pytest.fixture
def restore_spf_device_operating_mode(spf, request):
    """
    A teardown function which will ensure
    that the spf in ITF is restored to LP mode
    """

    def set_standby_lp_mode():
        LOGGER.info(f"Restoring {spf} to STANDBY-LP mode")
        # The SPF Controller in the ITF is accessible to anyone to control
        # Set the SPFC operatingMode to the LP mode before exiting the tests
        if "simulator" not in spf.name():
            spf.SetStandbyLPMode()

    request.addfinalizer(set_standby_lp_mode)
