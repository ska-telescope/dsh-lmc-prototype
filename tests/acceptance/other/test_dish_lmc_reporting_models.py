# pylint: disable=missing-function-docstring
"""
Verify that dish_lmc supports reading attributes in 3 different ways

jira requirement: L2-4677
"""

import logging

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from ska_control_model import CommunicationStatus, ResultCode

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario("XTP-39181.feature", "Test Dish LMC Reporting Models")
def test_dish_lmc_supports_all_reporting_models(monitor_tango_servers):
    pass


@given("dish_manager establishes connection with dish_structure, spf and spfrx")
def check_dish_manager_is_connected_to_subservient_devices(dish_manager):
    conn_states = [
        dish_manager.spfConnectionState,
        dish_manager.spfrxConnectionState,
        dish_manager.dsconnectionstate,
    ]
    assert all(conn_state == CommunicationStatus.ESTABLISHED for conn_state in conn_states)


@when(parse("dish_manager receives a {desired_mode} mode request"), target_fixture="cmd_response")
def issue_standbyfp_request(desired_mode, dish_manager, modes_helper, modes_command_map):
    # ensure that DishManager is in standby lp
    modes_helper.ensure_dish_manager_mode("STANDBY_LP")

    dish_mode = desired_mode.replace("-", "_")
    command_name = modes_command_map[dish_mode]

    [[result_code], [_]] = dish_manager.command_inout(command_name)
    return result_code


@then("dish_manager reports that the command has been accepted")
def check_dish_manager_acknowledged_command(cmd_response):
    assert cmd_response == ResultCode.QUEUED


@then("dish_manager pushes change events on the dishMode attribute")
def check_dish_manager_pushes_change_events(dish_manager, event_store_class):
    """Test that dish manager supports attribute reads on events"""
    event_cb = event_store_class()

    # change events
    dish_manager.subscribe_event(
        "dishMode",
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    dish_mode_events = event_cb.get_queue_values()
    assert dish_mode_events
