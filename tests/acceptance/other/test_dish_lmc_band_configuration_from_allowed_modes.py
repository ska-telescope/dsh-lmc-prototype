# pylint: disable=C0116
# pylint: disable=E0102
"""
Verify that dish transitions to CONFIG from STANDBY_FP, STOW and OPERATE

jira requirement: L2-4588, L2-4612, L2-4621, L2-4627
"""

import logging

import pytest
import tango
from dish_enums import DishMode
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario("XTP-5703.feature", "Test dish lmc band selection")
def test_band_selection(monitor_tango_servers, init_devices):
    pass


@given(parse("dish_manager dishMode reports {dish_mode}"), target_fixture="data_store")
def dish_reports_allowed_dish_mode(
    dish_mode,
    dish_manager,
    event_store_class,
    modes_helper,
):
    dm_dish_mode_events = event_store_class()
    dish_manager.subscribe_event(
        "dishMode",
        tango.EventType.CHANGE_EVENT,
        dm_dish_mode_events,
    )

    dish_mode = dish_mode.replace("-", "_")
    modes_helper.ensure_dish_manager_mode(dish_mode)
    current_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    LOGGER.info(f"{dish_manager} initial dishMode: {current_dish_mode}")

    return current_dish_mode, dm_dish_mode_events


@when(parse("I issue ConfigureBand{band_number} on dish_manager"))
def configure_band(
    band_number,
    dish_freq_band_configuration,
):
    dish_freq_band_configuration.go_to_band(band_number)


@then(
    parse("dish_manager dishMode should report {transient_mode} briefly"),
    target_fixture="initial_dish_mode",
)
def check_dish_mode(transient_mode, dish_manager, data_store):
    initial_dish_mode, dm_dish_mode_events = data_store
    # convert dish mode to have underscore for enums in utils
    transient_mode = transient_mode.replace("-", "_")

    dm_dish_mode_events.wait_for_value(DishMode[transient_mode], timeout=10)
    LOGGER.info(f"{dish_manager} dishMode reported: {transient_mode}")

    return initial_dish_mode


@then(parse("dish_structure indexerPosition should report {band_number}"))
def check_ds_indexer_position_is_not_moving(band_number, dish_structure):
    # B5a and B5b are all returned as B5 for indexer_position
    band_number = 5 if band_number.startswith("5") else band_number
    indexer_position = retrieve_attr_value(dish_structure, "indexerPosition")
    assert indexer_position == f"B{band_number}"
    LOGGER.info(f"{dish_structure} indexerPosition: {indexer_position}")


@then(parse("spf bandInFocus should report {band_number}"))
def check_spf_band_in_focus(band_number, spf):
    # B5a and B5b are all returned as B5 for indexer_position
    band_number = 5 if band_number.startswith("5") else band_number
    band_in_focus = retrieve_attr_value(spf, "bandInFocus")
    assert band_in_focus == f"B{band_number}"
    LOGGER.info(f"{spf} bandInFocus: {band_in_focus}")


@then(parse("spfrx operatingMode should report {expected_mode}"))
def check_spfrx_operating_mode(expected_mode, spfrx):
    operating_mode = retrieve_attr_value(spfrx, "operatingMode")
    assert operating_mode.replace("-", "_") == expected_mode.replace("-", "_")
    LOGGER.info(f"{spfrx} operatingMode: {operating_mode}")


@then(parse("spfrx configuredBand should report {band_number}"))
def check_spfrx_configured_band(band_number, spfrx):
    configured_band = retrieve_attr_value(spfrx, "configuredBand")
    assert configured_band == f"B{band_number}"
    LOGGER.info(f"{spfrx} configuredBand: {configured_band}")


@then(parse("dish_manager configuredBand should report {band_number}"))
def check_dish_configured_band(band_number, dish_manager):
    configured_band = retrieve_attr_value(dish_manager, "configuredBand")
    assert configured_band == f"B{band_number}"
    LOGGER.info(f"{dish_manager} configuredBand: {configured_band}")


@then("dish_manager should report its initial dishMode")
def check_dish_mode(dish_manager, initial_dish_mode):  # noqa: F811
    dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    assert dish_mode == initial_dish_mode
    LOGGER.info(f"{dish_manager} dishMode: {dish_mode}")
