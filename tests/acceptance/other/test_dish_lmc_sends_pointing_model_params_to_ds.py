"""
Verify that dish lmc updates static pointing model parameters

jira requirement: L2-5121, L2-4704
"""

import json
import logging

import pytest
import tango
from pytest_bdd import given, scenario, then, when
from pytest_bdd.parsers import parse

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario(
    "XTP-28120.feature",
    "Dish LMC is able to update pointing model parameters regardless of configuredBand",
)
def test_dish_lmc_updates_pointing_model_parameters():
    # pylint: disable=missing-function-docstring
    pass


@given("dish_manager and dish_structure have attributes for setting pointing model parameters")
def dish_manager_has_attribute(dish_manager, dish_structure):
    # pylint: disable=missing-function-docstring
    dish_manager_pointing_model_attrs = [
        "band1PointingModelParams",
        "band2PointingModelParams",
        "band3PointingModelParams",
        "band4PointingModelParams",
        "band5aPointingModelParams",
        "band5bPointingModelParams",
    ]

    # currently implementing only one pointing model parameter in DSManager
    # TODO (ST 04/2024): Expand pointing model list in DSManager for test
    ds_manager_pointing_model_attrs = [
        "band1PointingModelParams",
        "band2PointingModelParams",
        "band3PointingModelParams",
        "band4PointingModelParams",
    ]

    dish_manager_attrs = dish_manager.get_attribute_list()
    ds_manager_attrs = dish_structure.get_attribute_list()

    assert set(dish_manager_pointing_model_attrs).issubset(dish_manager_attrs)
    assert set(ds_manager_pointing_model_attrs).issubset(ds_manager_attrs)


@when(parse("I write {value} to {pointing_model_parameter}"), target_fixture="ds_param_val")
def write_to_pointing_model_parameter(value, pointing_model_parameter, dish_manager):
    # pylint: disable=missing-function-docstring
    value = json.loads(value)
    dish_manager.write_attribute(pointing_model_parameter, value)
    return value


@then(parse("the dish_structure {pointing_model_parameter} is updated"))
def check_ds_static_parameters_are_updated(
    ds_param_val, pointing_model_parameter, dish_structure, event_store_class
):
    # pylint: disable=missing-function-docstring
    event_cb = event_store_class()
    dish_structure.subscribe_event(
        pointing_model_parameter,
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    # wait for the DSC updates to be sent through to DS Manager
    event_cb.wait_for_value(ds_param_val, timeout=5)


@then(parse("dish_manager {pointing_model_parameter} should report {expected_value}"))
def check_dish_manager_pointing_model_parameter_update(
    pointing_model_parameter, expected_value, dish_manager, event_store_class
):
    # pylint: disable=missing-function-docstring
    event_cb = event_store_class()
    dish_manager.subscribe_event(
        pointing_model_parameter,
        tango.EventType.CHANGE_EVENT,
        event_cb,
    )
    expected_value = json.loads(expected_value)
    # wait for the DSManager updates to be sent through to Dish Manager
    event_cb.wait_for_value(expected_value, timeout=5)
