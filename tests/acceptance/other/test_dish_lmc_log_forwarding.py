"""
Verify that the logs from dish subsystems are forwarded via TLS to ELK
"""

import pytest
import tango


@pytest.mark.lmc_logger
def test_dish_lmc_simulator_devices_log_to_dish_logger(init_devices, spf, spfrx):
    """Test that SPF and SPFRx simulators log to DishLogger"""
    spf_dev_name = spf.name()
    spfrx_dev_name = spfrx.name()

    # add dish_logger as a target on the simulator devices
    logger_dev_name = "mid-dish/dish-logger/SKA001"
    dish_logger = tango.DeviceProxy(logger_dev_name)
    dish_logger.SetDishLoggerTarget(spf_dev_name)
    dish_logger.SetDishLoggerTarget(spfrx_dev_name)

    # trigger commands on both spf and spfrx devices to emit logs
    # the logs can be further explored from kibana tests complete
    spf.SetStandbyLPMode()
    spfrx.SetStandbyMode()

    # We only need proof that logs go from TLS to ELK. For the purposes of this test
    # one log from each device is sufficient. Remove device as a logging target

    # remove dish_logger as a target on the simulator devices
    dish_logger.RemoveDishLoggerTarget(spf_dev_name)
    dish_logger.RemoveDishLoggerTarget(spfrx_dev_name)
