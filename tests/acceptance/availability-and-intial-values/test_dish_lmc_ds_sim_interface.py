"""Test whether the DS simulator has the necessary attributes"""

# pylint: disable=too-many-function-args,unexpected-keyword-arg
import pytest
from asyncua import ua
from asyncua.sync import Client


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "node_sub_paths",
    [
        (["Management"]),
        (["Management", "Status"]),
        (["Management", "Status", "DscState"]),
        (["Management", "Status", "FiPos"]),
        (["Management", "PowerStatus", "LowPowerActive"]),
        (["FeedIndexer", "Status", "AxisMoving"]),
        (["FeedIndexer", "Status", "AxisState"]),
        (["Azimuth", "Status", "AxisState"]),
        (["Elevation", "Status", "AxisState"]),
        (["Tracking", "Status", "p_desired_Az"]),
        (["Tracking", "Status", "p_desired_El"]),
        (["Pointing", "Status", "BandForCorr"]),
        (["Pointing", "StaticPmParameters"]),
        (["Tracking", "Commands", "TrackLoadStaticOff"]),
        (["Tracking", "Commands", "TrackLoadTable"]),
        (["Tracking", "Commands", "TrackStart"]),
        (["Pointing", "Commands", "PmCorrOnOff"]),
        (["Management", "Commands", "Stow"]),
        (["Management", "Commands", "SetPowerMode"]),
        (["Management", "Commands", "Activate"]),
        (["Management", "Commands", "DeActivate"]),
        (["Management", "Commands", "Move2Band"]),
        (["Management", "Commands", "Slew2AbsAzEl"]),
        (["Management", "Commands", "Stop"]),
    ],
)
def test_ds_simulator_has_correct_attr(node_sub_paths, ds_simulator_url, ds_simulator_namespace):
    """Tests if all neccessary DS sim nodes are available"""
    client = Client(url=ds_simulator_url)
    client.connect()
    namespace_idx = client.get_namespace_index(uri=ds_simulator_namespace)
    req_node_path = [
        "0:Objects",
        f"{namespace_idx}:Logic",
        f"{namespace_idx}:Application",
        f"{namespace_idx}:PLC_PRG",
    ]
    # PLC_PRG is the head node and all other nodes extends it opc ua path.
    for sub_path in node_sub_paths:
        req_node_path.append(f"{namespace_idx}:{sub_path}")

    try:
        requested_node = client.nodes.root.get_child(req_node_path)
        assert requested_node
    except ua.uaerrors.BadNoMatch as e:
        raise Exception(f"Cannot get {req_node_path}") from e
    finally:
        client.disconnect()
