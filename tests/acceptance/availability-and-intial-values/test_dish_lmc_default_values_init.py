# pylint: disable=missing-function-docstring
"""
Verify that the startup values for dish and the subelements are accurate

jira requirement: L2-4633 L2-4691
"""

import logging

import pytest
from pytest_bdd import given, scenario, then
from pytest_bdd.parsers import parse
from utils import retrieve_attr_value

LOGGER = logging.getLogger(__name__)


@pytest.mark.acceptance
@scenario("XTP-37884.feature", "Test Dish LMC Dish Startup (STARTUP Mode)")
def test_dish_lmc_startup(monitor_tango_servers):
    pass


@given(
    parse("either dish_structure, spf or spfrx operatingMode reports {operating_mode}"),
    target_fixture="is_sub_device_startup",
)
def check_sub_elements_expected_operating_mode(operating_mode, dish_structure, spf, spfrx):
    operating_mode = operating_mode.replace("-", "_")
    dev_proxies = [dish_structure, spf, spfrx]
    return any(operating_mode == retrieve_attr_value(dp, "operatingMode") for dp in dev_proxies)


@then(parse("dish_manager dishMode reports {dish_mode}"))
def check_dish_manager_startups_with_expected_dish_mode(
    is_sub_device_startup, dish_mode, dish_manager
):
    """Test that dish manager starts up with the expected dishMode"""
    actual_dish_mode = retrieve_attr_value(dish_manager, "dishMode")
    if is_sub_device_startup:
        assert actual_dish_mode == dish_mode
    else:
        assert actual_dish_mode != dish_mode
