"""Tests the population of Dish LMC release info in the Dish logger buildState attribute"""

import json

import pytest

from ska_dish_lmc import release


@pytest.mark.smoke_test
def test_dish_lmc_build_state_populated(dish_logger):
    """Test buildState attribute fields populated on Dish Logger."""
    build_state_attr = dish_logger.read_attribute("buildState")
    lmc_buildstate_dict = json.loads(build_state_attr.value)

    assert lmc_buildstate_dict["package"] == release.name
    assert lmc_buildstate_dict["release_version"] == release.get_dish_lmc_release_version()
