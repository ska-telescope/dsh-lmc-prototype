"""
Smoke test to see if tango db and devices are available.
This runs on k8s test job, psi and itf clusters.
"""

import pytest
import tango
from dish_enums import DishMode


@pytest.mark.parametrize("member_id_instance", ["001", "111"])
@pytest.mark.parametrize(
    "family", ["dish-manager", "ds-manager", "simulator-spfc", "simulator-spfrx", "dish-logger"]
)
def test_lmc_devices_available_for_multiple_instances(
    member_id_instance, family, event_store_class
):
    """Test that dish lmc devices are available in skampi and responsive"""
    tango_device_proxy = tango.DeviceProxy(f"mid-dish/{family}/SKA{member_id_instance}")
    assert isinstance(tango_device_proxy.ping(), int)
    # Just make sure the device responds
    assert tango_device_proxy.State() in tango.DevState.values.values()

    if family == "dish-manager":
        dish_mode_event_store = event_store_class()
        dish_mode_event_store.clear_queue()

        tango_device_proxy.subscribe_event(
            "dishMode",
            tango.EventType.CHANGE_EVENT,
            dish_mode_event_store,
        )

        dish_mode_event_store.wait_for_value(DishMode.STANDBY_LP, timeout=5)
        assert tango_device_proxy.dishMode == DishMode.STANDBY_LP


@pytest.mark.smoke_test
def test_tango_db_is_reachable():
    """Test if Tango DB is pingable."""
    device_proxy = tango.DeviceProxy("sys/database/2")
    assert device_proxy.ping()


@pytest.mark.smoke_test
@pytest.mark.parametrize(
    "dev_name",
    ["dish_manager_fqdn", "dish_structure_fqdn", "spf_fqdn", "spfrx_fqdn"],
    indirect=["dev_name"],
)
def test_lmc_devices_available_in_local_deployment(dev_name):
    """Test that dish lmc devices are available in local repo deployment and responsive"""
    tango_device_proxy = tango.DeviceProxy(dev_name)
    assert isinstance(tango_device_proxy.ping(), int)
    # Just make sure the device responds
    assert tango_device_proxy.State() in tango.DevState.values.values()
